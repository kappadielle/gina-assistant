import { DocumentReference } from 'angularfire2/firestore';


export interface User {
    email?: string;
    phoneNumber?: any;
    photoUrl?: string;
    codFis?: string;
    id?: string;  // non serve per inserimento ma per controllo documento e comodità
    medicRef?: DocumentReference;
    medico?: boolean;
    nome?: string;
    cognome?: string;
    sedeRef?: string;
    accountCrowd?: boolean;
}
