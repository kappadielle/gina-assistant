import { Component, OnInit, OnDestroy } from '@angular/core';
import { AutenticationService } from './autentication.service';
import { Medico } from './medico';
import { User } from './user';
import { Observable, Subscription, of } from 'rxjs';
import { filter } from 'rxjs/operators';
import { trigger, state, style, transition, animate, query} from '@angular/animations';
import * as Particles from 'particlesjs';
import { UserDataService } from './user-data.service';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import { RouterOutlet } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('openClose', [
      state('open', style({
        height: '65px',
        opacity: 1,
        backgroundColor: '#74a4f2'
      })),
      state('closed', style({
        height: '0px',
        opacity: 0,
        backgroundColor: '#bdbdbd'
      })),
      transition('open => closed', [
        animate(250)
      ]),
      transition('void => open', [
        animate('0.2s')
      ]),
      transition('closed => open', [
        animate(250)
      ]),
    ]),
    trigger('routeAnimations', [
      transition('* <=> *', [
        query(':enter, :leave', [
          style({ 
            position: 'absolute',
            left: 0,
            width: '100%',
            opacity: 0,
            transform: 'scale(0) translateY(100%)'
          }),
        ], {optional: true}),
        query(':enter', [
          animate('600ms ease',
            style({ opacity: 1, transform: 'scale(1) translateY(0)' })
          )
        ], {optional: true})
      ])    
    ])
  ]
})
export class AppComponent implements OnInit, OnDestroy {
  settings = true;
  currentUser: User;
  hero$: Observable<any>;
  medico: Medico;
  subUserData: Subscription;
  myStorage: any;
  logoImg = '../assets/logoscritta.png';
  navSelect = false;
  subResponsive: Subscription;
  // tslint:disable-next-line:max-line-length
  constructor(private authSer: AutenticationService, public dataSer: UserDataService, breakpointObserver: BreakpointObserver) {
    // authSer.medicoLogin = true;
    // authSer.verificaTipoLogin();
    this.subResponsive = breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.TabletPortrait,
    ]).subscribe(result => {
      if (result.matches) {
        if (result.breakpoints) {
          if (result.breakpoints['(min-width: 600px) and (max-width: 839.99px) and (orientation: portrait)']) {
            // console.log(result.breakpoints['(min-width: 600px) and (max-width: 839.99px) and (orientation: portrait)']);
          } else if (result.breakpoints['(max-width: 599.99px)']) {
            // console.log(result.breakpoints['(max-width: 599.99px)']);
          }
        }
        this.logoImg = '../assets/logo.png';
        this.navSelect = true;
      } else {
        this.navSelect = false;
        this.logoImg = '../assets/logoscritta.png';
      }
    });
    this.dataSer.updateVisitCounter();
  }                                                      // per prove con medico:
                                                        // email: medico@prova.it
                                                        // password: ciaociao1
  ngOnInit() {
    this.dataSer.verificaModalitaSala();
    if (this.dataSer.userData) {
      this.currentUser = this.dataSer.user;
      this.medico = this.dataSer.medico;
      if (!this.currentUser.medico && this.dataSer.modSala == 'true') {
        this.dataSer.myStorage.setItem('modSala', 'false');
        this.dataSer.modSala = 'false';
      }
    } else {
      this.subUserData = this.dataSer.userData$.pipe(
        filter(val => val !== undefined)
      )
      .subscribe(user => {
        this.currentUser = this.dataSer.user;
        this.medico = this.dataSer.medico;
        if (!this.currentUser.medico && this.dataSer.modSala == 'true') {
          this.dataSer.myStorage.setItem('modSala', 'false');
          this.dataSer.modSala = 'false';
        }
      });
    }
    Particles.init({
      selector: '#background',
      maxParticles: 220,
      connectParticles: true,
      color: ['#87B7FF'],
      sizeVariations: 3,
      // color: ['rgb(135, 183, 255)','rgb(214, 242, 255)'],
      // options for breakpoints
      responsive: [
        {
          breakpoint: 768,
          options: {
            maxParticles: 120,
            connectParticles: true
          }
        }, {
          breakpoint: 425,
          options: {
            maxParticles: 55,
            connectParticles: true
          }
        }, {
          breakpoint: 320,
          options: {
            maxParticles: 35
          }
        }
      ]
    });
  }
  logOut() {
    this.currentUser = null;
    this.dataSer.medico = null;
    this.dataSer.user = null;
    this.dataSer.userData = null;
    this.authSer.logOut();
  }
  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
  ngOnDestroy() {
    if (this.subUserData) { this.subUserData.unsubscribe(); }
    if (this.subResponsive) { this.subResponsive.unsubscribe(); }
  }
}
