import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { UserDataService } from '../user-data.service';
import { CalendarioService } from '../calendario.service';
import { Medico } from '../medico';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-appuntamenti-manager',
  templateUrl: './appuntamenti-manager.component.html',
  styleUrls: ['./appuntamenti-manager.component.css']
})
export class AppuntamentiManagerComponent implements OnInit, OnDestroy {
  modSala: string;
  medico: Medico;
  subUserData: Subscription;

  constructor(private dataSer: UserDataService, private CDR: ChangeDetectorRef, public calSer: CalendarioService) { }

  ngOnInit() {
    if (this.dataSer.userData) {
      this.medico = this.dataSer.userData;
      if (!this.medico.orarioRicette) { this.medico.orarioRicette = ''; }
      if (!this.medico.durataAppuntamenti) { this.medico.durataAppuntamenti = 20; }
      this.modSala = this.dataSer.modSala;
    } else {
      this.subUserData = this.dataSer.userData$.pipe(
        filter(val => val != null)
      )
      .subscribe(user => {
        this.medico = user;
        if (!this.medico.orarioRicette) { this.medico.orarioRicette = ''; }
        if (!this.medico.durataAppuntamenti) { this.medico.durataAppuntamenti = 20; }
        this.modSala = this.dataSer.modSala;
      });
    }
    this.CDR.detectChanges();
  }
  setDurataAppuntamenti(event) {
    if (event.target.value) {
      this.medico.durataAppuntamenti = event.target.value;
      this.calSer.setDurataAppuntamenti(event);
    }
  }
  ngOnDestroy(): void {
    if (this.subUserData) { this.subUserData.unsubscribe(); }
  }
}
