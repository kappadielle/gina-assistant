import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppuntamentiManagerComponent } from './appuntamenti-manager.component';

describe('AppuntamentiManagerComponent', () => {
  let component: AppuntamentiManagerComponent;
  let fixture: ComponentFixture<AppuntamentiManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppuntamentiManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppuntamentiManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
