import { Component, OnInit, ChangeDetectorRef, Input, OnDestroy } from '@angular/core';
import { CalendarioService } from '../calendario.service';
import { UserDataService } from '../user-data.service';
import { Subscription } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';
@Component({
  selector: 'app-orari',
  templateUrl: './orari.component.html',
  styleUrls: ['./orari.component.css']
})
export class OrariComponent implements OnInit, OnDestroy {
  medico: import('./../medico').Medico;
  subOrari: Subscription;
  subEventiApp: Subscription;
  subUserData: Subscription;
  constructor(private dataSer: UserDataService, public calSer: CalendarioService, private CDR: ChangeDetectorRef) { }
  @Input() tipo: string;
  ngOnInit() {
    if (this.dataSer.userData) {
      this.medico = this.dataSer.medico;
      this.subOrari = this.calSer.getOrarioSede(this.medico.id).pipe(take(1)).subscribe(_ => {
        if (this.calSer.orari['lun'] === undefined) {
          this.azzeraOrario('attivita');
        }
        if (this.calSer.orariAppuntamenti['lun'] === undefined) {
          this.azzeraOrario('appuntamenti');
        }
      });
      this.subEventiApp = this.calSer.getEventiSede(this.medico.id).subscribe();
    } else {
      this.subUserData = this.dataSer.userData$.pipe(
        mergeMap(val =>
          this.calSer.getEventiSede(this.dataSer.medico.id)
        ),
        mergeMap(val =>
          this.calSer.getOrarioSede(this.dataSer.medico.id).pipe(take(1))
        ),
      ).subscribe(val => {
        this.medico = this.dataSer.medico;
        if (this.calSer.orari['lun'] === undefined) {
          this.azzeraOrario('attivita');
        }
        if (this.calSer.orariAppuntamenti['lun'] === undefined) {
          this.azzeraOrario('appuntamenti');
        }
      });
    }
  }
  cambiaOrarioApertura(e, str: string, tipo: string = 'attivita') {
    this.CDR.detectChanges();
    this.calSer.cambiaOrarioApertura(e, str, tipo);
  }
  cambiaOrarioChiusura(e, str: string, tipo: string = 'attivita') {
    this.CDR.detectChanges();
    this.calSer.cambiaOrarioChiusura(e, str, tipo);
  }
  azzeraOrario(tipo: string) {
    this.calSer.azzeraOrario(tipo);
  }
  dinamicObjGiornoSett(day: string, open_close: string, val: string, tipo: string): any {
    // ritorna oggetto lun-mar-mer2... e reimposta orari se sballati
    this.CDR.detectChanges();
    return this.calSer.dinamicObjGiornoSett(day, open_close, val, tipo);
  }
  cambiaAttrOrari(day: string, open_close: string, val: string, tipo: string ) { // cambia un attributo alla volta negli orari
    this.CDR.detectChanges();
    this.calSer.cambiaAttrOrari(day, open_close, val, tipo);
  }
  ngOnDestroy(): void {
    if (this.subEventiApp) { this.subEventiApp.unsubscribe(); }
    if (this.subUserData) { this.subUserData.unsubscribe(); }
  }
}

