import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickerAppuntamentiComponent } from './picker-appuntamenti.component';

describe('PickerAppuntamentiComponent', () => {
  let component: PickerAppuntamentiComponent;
  let fixture: ComponentFixture<PickerAppuntamentiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickerAppuntamentiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickerAppuntamentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
