import { Component, OnInit, OnDestroy } from '@angular/core';
import { CalendarioService } from '../calendario.service';
import { MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import * as _moment from 'moment';
import { FormControl } from '@angular/forms';
import { Medico } from '../medico';
import { mergeMap, take } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { UserDataService } from '../user-data.service';
import { User } from '../user';
import { Router } from '@angular/router';
const moment =  _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'MM YYYY',
    monthYearA11yLabel: 'DD MM YYYY',
  },
};

@Component({
  selector: 'app-picker-appuntamenti',
  templateUrl: './picker-appuntamenti.component.html',
  styleUrls: ['./picker-appuntamenti.component.css'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class PickerAppuntamentiComponent implements OnInit, OnDestroy {
  date = new FormControl(moment());
  dataAppPicker: Date;
  medico: Medico;
  subUserData: Subscription;
  subOrari: Subscription;
  subEventiApp: Subscription;
  user: User;
  puoiprenota = true;
  messaggio: string;
  oggi = new Date();
  // tslint:disable-next-line:max-line-length
  constructor(public calSer: CalendarioService, public dataSer: UserDataService, private router: Router) { }

  ngOnInit() {
    if (this.dataSer.userData) {
      this.user = this.dataSer.user;
      this.medico = this.dataSer.medico;
      if (!this.user.nome) {this.user.nome = ''; }
      if (!this.user.cognome) {this.user.cognome = ''; }
      this.subOrari = this.calSer.getOrarioSede(this.dataSer.medico.id).pipe(take(1)).subscribe(_ => {
        this.dataSer.sedeOpen = this.calSer.checkStudioAperto();
      });
      this.subEventiApp = this.calSer.getEventiSede(this.medico.id, false).subscribe();
    } else {
      this.subUserData = this.dataSer.userData$.pipe(
        mergeMap(val =>
          this.calSer.getEventiSede(this.dataSer.medico.id, false)
        ),
        mergeMap(val =>
          this.calSer.getOrarioSede(this.dataSer.medico.id).pipe(take(1))
        ),
      ).subscribe(val => {
        this.dataSer.sedeOpen = this.calSer.checkStudioAperto();
        this.user = this.dataSer.user;
        this.medico = this.dataSer.medico;
        if (!this.user.nome) {this.user.nome = ''; }
        if (!this.user.cognome) {this.user.cognome = ''; }
      });
    }
  }
  prenotaAppuntamento() {
    if (this.user.nome.length > 2 && this.user.cognome.length > 3) {
      this.setNomeUser();
      this.setCognomeUser();
    }
    this.puoiprenota = true;
    this.router.navigate(['/home']);
    this.calSer.prenotaAppuntamento(this.messaggio);
  }
  setDataAppuntamento($event) {
    this.calSer.setDataAppuntamento($event);
  }
  setOrarioAppuntamento($event) {
    this.calSer.setOrarioAppuntamento($event);
    this.puoiprenota = false;
  }
  setNomeUser() {
    this.dataSer.setNomeUser(this.user.nome);
    this.user.nome = this.user.nome;
  }
  setCognomeUser() {
    this.dataSer.setCognomeUser(this.user.cognome);
    this.user.cognome = this.user.cognome;
  }
  ngOnDestroy(): void {
    if (this.subEventiApp) { this.subEventiApp.unsubscribe(); }
    if (this.subUserData) { this.subUserData.unsubscribe(); }
  }
}
