import { Orario } from './orario';
export interface Orari {
    lun?: Orario;
    mar?: Orario;
    mer?: Orario;
    gio?: Orario;
    ven?: Orario;
    sab?: Orario;
    dom?: Orario;
    lun2?: Orario;
    mar2?: Orario;
    mer2?: Orario;
    gio2?: Orario;
    ven2?: Orario;
    sab2?: Orario;
    dom2?: Orario;
}
