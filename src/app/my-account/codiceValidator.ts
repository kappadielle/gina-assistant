import { AsyncValidator, AbstractControl, ValidationErrors, NG_ASYNC_VALIDATORS, AsyncValidatorFn } from '@angular/forms';
import { Directive, Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { UserDataService } from '../user-data.service';
import { map, catchError, take } from 'rxjs/operators';

export function codiceValidator(dataSer): AsyncValidatorFn {
    return (c: AbstractControl): Observable<{ [key: string]: any } | null> => {
        return c.value && c.value.length === 5 ? dataSer.checkEsistenzaSede({ target: { value: c.value } }).pipe(
            map(val => {
                return val ? null : { 'codiceTaken': true };
            }),
            take(1),
            catchError(() => null)
        ) : of('');
    };
}

@Injectable({ providedIn: 'root' })
@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[codiceValidator]',
    providers: [{ provide: NG_ASYNC_VALIDATORS, useExisting: CodiceValidatorDirective, multi: true }]
})

export class CodiceValidatorDirective implements AsyncValidator {

    constructor(private dataSer: UserDataService) { }

    validate(c: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
        return c.value && c.value.length === 5 ? this.dataSer.checkEsistenzaSede({ target: { value: c.value } }).pipe(
            map(val => {
                return val ? { 'codiceTaken': true } : null;
            }),
            take(1),
            catchError(() => null)
        ) : of('');
    }

}
