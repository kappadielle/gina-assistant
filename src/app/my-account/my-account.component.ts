import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { UserDataService } from '../user-data.service';
import { AutenticationService } from '../autentication.service';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { User } from '../user';
import { FormBuilder, Validators } from '@angular/forms';
import { TooltipPosition } from '@angular/material';
import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { codiceValidator } from './codiceValidator';
@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit, OnDestroy {
  medico: any;
  subUserData: Subscription;
  user: User;
  tooltipPosition: TooltipPosition = 'right';

  // tslint:disable-next-line:max-line-length
  constructor(private fb: FormBuilder, private authSer: AutenticationService, public dataSer: UserDataService, private bottomSheet: MatBottomSheet) { }
  codFisForm = this.fb.group({
    codFis:       ['', [Validators.pattern('[A-Za-z]{6}[0-9]{2}[A-Za-z]{1}[0-9]{2}[A-Za-z]{1}[0-9]{3}[A-Za-z]{1}')]],
    nome:         ['', [Validators.required, Validators.minLength(3)]],
    cognome:      ['', [Validators.required, Validators.minLength(4)]],
    codSede:      ['', Validators.compose(
                    [Validators.required],
                  ),
                  // custom async validator
                  codiceValidator(this.dataSer)]
                  // [Validators.required, codiceValidator(this.dataSer)]],
  });
  get codFis() { return this.codFisForm.get('codFis'); }
  get nome() { return this.codFisForm.get('nome'); }
  get cognome() { return this.codFisForm.get('cognome'); }
  get codSede() { return this.codFisForm.get('codSede'); }
  ngOnInit() {
    if (this.dataSer.userData) {
      this.medico = this.dataSer.medico;
      this.user = this.dataSer.userData;
      if (!this.user.codFis) { this.user.codFis = ''; } // updato la validità dei form perchè senno non se li prendeva
      this.codFis.setValue(this.user.codFis);          // il valore nell' html
      this.codFis.updateValueAndValidity();
      this.nome.setValue(this.user.nome);
      this.nome.updateValueAndValidity();
      this.cognome.setValue(this.user.cognome);
      this.cognome.updateValueAndValidity();
      this.codSede.setValue(this.user.sedeRef);
      this.codSede.updateValueAndValidity();
    } else {
      this.subUserData = this.dataSer.userData$.pipe(
        filter(val => val != null)
      )
      .subscribe(user => {
        this.medico = this.dataSer.medico;
        this.user = user;
        if (!this.user.codFis) { this.user.codFis = ''; }
        this.codFis.setValue(this.user.codFis);
        this.codFis.updateValueAndValidity();
        this.nome.setValue(this.user.nome);
        this.nome.updateValueAndValidity();
        this.cognome.setValue(this.user.cognome);
        this.cognome.updateValueAndValidity();
        this.codSede.setValue(this.user.sedeRef);
        this.codSede.updateValueAndValidity();
        // this.subUserData.unsubscribe();
      });
    }
  }
  logOut() {
    this.medico = null;
    this.user = null;
    this.dataSer.medico = null;
    this.dataSer.user = null;
    this.dataSer.userData = null;
    this.authSer.logOut();
    // this.subWhenChecked.unsubscribe();
  }
  cambiaSede(sede) {
    this.medico.sedeRef = sede;
    this.user.sedeRef = sede;
    this.dataSer.cambiaSede(sede);
  }
  aggiungiNuovaSede() {
    this.bottomSheet.open(EventInfoModalsede, {data: ''});
    // this.dataSer.aggiungiNuovaSede();
  }
  /*checkEsistenzaSede($event) {
    const obsMedLinked = this.dataSer.checkEsistenzaSede($event);
    if (obsMedLinked) {
      obsMedLinked.subscribe(val => {
        if (val.length > 0) {
          this.codFisForm.controls['codSede'].setErrors(null);
          this.sedeRef = $event.target.value; // assegna valori alla variabile locale cosi
          this.medicRef = val[0].medicRef; // le ho come variabili di classe e le richiamo a modificaCampi()
          // this.dataSer.linkMedicoAndSede(this.dataSer.user.id, this.medicRef, this.sedeRef); la chiamo a "modificaCampi()"
        } else {
          this.codFisForm.controls['codSede'].setErrors({'incorrect': true});
        }
      });
    }
  }*/
  attivaModSala() {
    if (this.dataSer.modSala === 'true') {
      this.dataSer.myStorage.setItem('modSala', 'false');
      this.dataSer.modSala = 'false';
    } else {
      this.dataSer.myStorage.setItem('modSala', 'true');
      this.dataSer.modSala = 'true';
    }
  }
  setNomeUser($event) {
    this.dataSer.setNomeUser($event);
    this.user.nome = $event;
  }
  setCognomeUser($event) {
    this.dataSer.setCognomeUser($event);
    this.user.cognome = $event;
  }
  setcodFisUser(codFis) {
    this.dataSer.setcodFisUser(codFis);
    this.user.codFis = codFis;
  }
  modificaCampi() {
    if (this.user.cognome !== this.cognome.value) { this.setCognomeUser(this.cognome.value); }
    if (this.user.nome !== this.nome.value) { this.setNomeUser(this.nome.value); }
    if (this.user.codFis !== this.codFis.value) { this.setcodFisUser(this.codFis.value); }
    // tslint:disable-next-line:max-line-length
    if (this.user.sedeRef !== this.codSede.value) { this.dataSer.linkMedicoAndSede(this.dataSer.user.id, this.dataSer.medicRef, this.dataSer.sedeRef); }
    this.user.sedeRef = this.dataSer.sedeRef; // assegna valori alla variabile locale cosi
    this.user.medicRef = this.dataSer.medicRef; // le ho come variabili di classe e le richiamo a modificaCampi()
  }
  ngOnDestroy(): void {
    if (this.subUserData) { this.subUserData.unsubscribe(); }
  }
}
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bottom-modal-event-sede',
  templateUrl: 'bottom-modal-event-sede.html',
})
// tslint:disable-next-line:component-class-suffix
export class EventInfoModalsede {
  sedeRef: any;
  // tslint:disable-next-line:max-line-length
  constructor(private bottomSheetRef: MatBottomSheetRef<EventInfoModalsede>, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
  private dataSer: UserDataService) {
    console.log(data);
  }
  codSede: string;
  aggiungiNuovaSede(name: string) {
    this.dataSer.aggiungiNuovaSede(name);
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
  openLink(event: MouseEvent): void {
    console.log('link open');
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
}
