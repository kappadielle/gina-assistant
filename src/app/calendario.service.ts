import { Injectable } from '@angular/core';
import { DBmanagerService } from './dbmanager.service';
import { Observable, Subject, Subscription, fromEvent } from 'rxjs';
import { EventDB } from './eventDB';
import { User } from 'firebase';
import { Medico } from './medico';
import { Orari } from './orari';
import { filter, take, map } from 'rxjs/operators';
import { CalendarEvent, DayViewHourSegment } from 'calendar-utils';
import { UserDataService } from './user-data.service';
import * as appPicker from 'appointment-picker';
import { OrderByDirection } from '@google-cloud/firestore';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';

@Injectable({
  providedIn: 'root'
})
export class CalendarioService {
  userData: User | Medico;
  userData$: Subject<User | Medico>;
  orari$: Observable<Orari>;
  eventi$: Observable<EventDB[]>;
  eventi: EventDB[];
  orariApp$: Observable<Orari>;
  subOrari: Subscription;
  subOrariApp: Subscription;
  events: any[];
  viewDate: Date = new Date();
  refresh: Subject<any> = new Subject();
  newAppuntamento: string[] = ['data', 'ora'];
  disattivaPicker = true;
  prenotaDisabled = true;
  smartList = false;
  colors: any = {
    red: {
      primary: '#3f51b5',
      secondary: '#5067E5'
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF'
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA'
    },
    disabled: {
      primary: '#f1f1f1',
      secondary: '#f1f1f1'
    }
  };
  orari: any[] = ['lun', 'mar', 'mer', 'gio', 'ven', 'sab', 'dom', 'lun2', 'mar2', 'mer2', 'gio2', 'ven2', 'sab2', 'dom2'];
  orariAppuntamenti: any[] = ['lun', 'mar', 'mer', 'gio', 'ven', 'sab', 'dom', 'lun2', 'mar2', 'mer2', 'gio2', 'ven2', 'sab2', 'dom2'];
  picker: any;
  isLargeOrariPicker = true;
  subResponsive: Subscription;
  constructor(private dataSer: UserDataService, private DB: DBmanagerService, breakpointObserver: BreakpointObserver) {
    this.subResponsive = breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.TabletPortrait,
    ]).subscribe(result => {
      if (result.matches) {
        if (result.breakpoints) {
          if (result.breakpoints['(min-width: 600px) and (max-width: 839.99px) and (orientation: portrait)']) {
            console.log(result.breakpoints['(min-width: 600px) and (max-width: 839.99px) and (orientation: portrait)']);
          } else if (result.breakpoints['(max-width: 599.99px)']) {
            console.log(result.breakpoints['(max-width: 599.99px)']);
          }
        }
        this.isLargeOrariPicker = false;
      }
    });
  }
  updateInfo(id: string, info: string): any {
    this.DB.updateDocument(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/appuntamenti/${id}`, {messaggio: info});
  }
  updateNominativo(id: string, nome: string): any {
    this.DB.updateDocument(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/appuntamenti/${id}`, {nominativo: nome});
  }
  checkStudioAperto(): boolean {
    const data = new Date();
    const dataMatchOpen = new Date();
    const dataMatchClose = new Date();
    let dayString = '';
    let returnVal = false;
    const dayW = data.getDay();
    switch (dayW) {
      case 1: dayString = 'lun'; break;
      case 2: dayString = 'mar'; break;
      case 3: dayString = 'mer'; break;
      case 4: dayString = 'gio'; break;
      case 5: dayString = 'ven'; break;
      case 6: dayString = 'sab'; break;
      case 0: dayString = 'dom'; break;
    }
    function leftPad(number, targetLength) {
      let output = number + '';
      while (output.length < targetLength) {
        output = '0' + output;
      }
      return output;
    }
    // tslint:disable-next-line:max-line-length
    if (!this.orari[dayString].open && !this.orari[dayString + '2'].open && !this.orari[dayString].close && !this.orari[dayString + '2'].close) {
      // non ci son orari ne di mattina ne pom
      returnVal = false;

    // tslint:disable-next-line:max-line-length
    } else if (this.orari[dayString].open && !this.orari[dayString + '2'].open && this.orari[dayString].close && !this.orari[dayString + '2'].close) {
      // ci son orari di mattina ma non pomeriggio
      dataMatchOpen.setHours(this.orari[dayString].open.substr(0, 2));
      dataMatchOpen.setMinutes(this.orari[dayString].open.substr(3, 2));
      dataMatchClose.setHours(this.orari[dayString].close.substr(0, 2));
      dataMatchClose.setMinutes(this.orari[dayString].close.substr(3, 2));
      if (dataMatchClose >= data && dataMatchOpen <= data) {
        returnVal = true;
      } else {
        returnVal = false;
      }
    // tslint:disable-next-line:max-line-length
    } else if (!this.orari[dayString].open && this.orari[dayString + '2'].open && !this.orari[dayString].close && this.orari[dayString + '2'].close) {
      // ci son orari di pomeriggio ma non di mattina
      dataMatchOpen.setHours(this.orari[dayString + '2'].open.substr(0, 2));
      dataMatchOpen.setMinutes(this.orari[dayString + '2'].open.substr(3, 2));
      dataMatchClose.setHours(this.orari[dayString + '2'].close.substr(0, 2));
      dataMatchClose.setMinutes(this.orari[dayString + '2'].close.substr(3, 2));
      if (dataMatchClose >= data && dataMatchOpen <= data) {
        returnVal = true;
      } else {
        console.log(data);
        returnVal = false;
      }
    // tslint:disable-next-line:max-line-length
    } else if (this.orari[dayString].open && this.orari[dayString + '2'].open && this.orari[dayString].close && this.orari[dayString + '2'].close) {
      // ci son orari sia di mattina che di pomeriggio
      dataMatchOpen.setHours(this.orari[dayString + '2'].open.substr(0, 2));
      dataMatchOpen.setMinutes(this.orari[dayString + '2'].open.substr(3, 2));
      dataMatchClose.setHours(this.orari[dayString + '2'].close.substr(0, 2));
      dataMatchClose.setMinutes(this.orari[dayString + '2'].close.substr(3, 2));
      if (dataMatchClose >= data && dataMatchOpen <= data) {
      } else {
        dataMatchOpen.setHours(this.orari[dayString].open.substr(0, 2));
        dataMatchOpen.setMinutes(this.orari[dayString].open.substr(3, 2));
        dataMatchClose.setHours(this.orari[dayString].close.substr(0, 2));
        dataMatchClose.setMinutes(this.orari[dayString].close.substr(3, 2));
        if (dataMatchClose >= data && dataMatchOpen <= data) {
          returnVal = true;
        } else {
          returnVal = false;
        }
      }
    }
    return returnVal;
  }
  setDataAppuntamento($event) {
    console.log(this.orariAppuntamenti);
    const val = $event.target.value;
    if (val !== undefined || val < new Date()) {
      this.newAppuntamento['data'] = val;
      const dayW = new Date(val).getDay();
      switch (dayW) {
        case 1: this.setOrariPicker ('lun', new Date(val)); break;
        case 2: this.setOrariPicker ('mar', new Date(val)); break;
        case 3: this.setOrariPicker ('mer', new Date(val)); break;
        case 4: this.setOrariPicker ('gio', new Date(val)); break;
        case 5: this.setOrariPicker ('ven', new Date(val)); break;
        case 6: this.setOrariPicker ('sab', new Date(val)); break;
        case 0: this.setOrariPicker ('dom', new Date(val)); break;
      }
    } else {
      this.dataSer.openSnackBar('Data non valida', '');
      this.disattivaPicker = true;
      this.newAppuntamento['data'] = '';
    }
  }
  setOrarioAppuntamento($event) {
    console.log($event.target.value);
    const val = $event.target.value;
    if (val !== undefined) {
      this.newAppuntamento['orario'] = val;
      this.prenotaDisabled = false;
    } else {
      this.prenotaDisabled = true;
      this.newAppuntamento['orario'] = '';
    }
  }
  getAppuntamento(id): Observable<EventDB> {
    const doc = this.DB.getDocument(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/appuntamenti/${id}`);
    return doc.valueChanges();
  }
  prenotaAppuntamento(messaggio: string) {
    if (this.newAppuntamento['orario'] && this.newAppuntamento['data']) {
      const start = new Date((new Date(this.newAppuntamento['data']).getTime() + this.newAppuntamento['orario'].substr(0, 2) * 60 * 60000)
                    + this.newAppuntamento['orario'].substr(3, 2) * 60000);
      const end = new Date(start.getTime() + this.dataSer.medico.durataAppuntamenti * 60000);
      const dragToSelectEvent: CalendarEvent = {
        id: this.events.length,
        title: 'New event',
        start: start,
        end: end,
        confermato: false
      };
      const inizioDate = new Date(dragToSelectEvent.start);
      const dateResetted = new Date(inizioDate);
      dateResetted.setHours(0, 0, 0, 0);   // Set hours, minutes and seconds
      const ref = this.dataSer.createUserRef();
      // tslint:disable-next-line:max-line-length
      this.DB.addToCollectionWithName(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/appuntamenti`, // metti dalla parte dell'user
      // tslint:disable-next-line:max-line-length
      {inizio: inizioDate, nominativo: this.dataSer.user.nome + ' ' + this.dataSer.user.cognome, userRef: ref, confermato: 'false', data: dateResetted, messaggio: messaggio}, 'app' + ref.id);
      this.events = [...this.events, dragToSelectEvent];
      this.subResponsive.unsubscribe();
    }
  }
  setOrariPicker (day, data: Date) {
    const dis = [];
    // tslint:disable-next-line:max-line-length
    const doc = this.DB.getQueryedCollection(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/appuntamenti`, 'data' , '==' , data);
    const obs = doc.valueChanges().pipe(take(1));
    obs.subscribe(val => {
      // tslint:disable-next-line:forin
      for (const item in val) {
        const datastartDB = new Date(val[item].inizio.seconds * 1000);
        const ora = datastartDB.getHours().toString();
        const min = datastartDB.getMinutes().toString();
        dis.push(leftPad(ora, 2) + ':' + leftPad(min, 2));
      }
      function leftPad(number, targetLength) {
        let output = number + '';
        while (output.length < targetLength) {
          output = '0' + output;
        }
        return output;
      }
      // tslint:disable-next-line:max-line-length
      if (!this.orariAppuntamenti[day].open && !this.orariAppuntamenti[day + '2'].open && !this.orariAppuntamenti[day].close && !this.orariAppuntamenti[day + '2'].close) {
        // non ci son eventi ne di mattina ne pom
        this.dataSer.openSnackBar('Il medico non lavora in questo dì', '');
      // tslint:disable-next-line:max-line-length
      } else if (this.orariAppuntamenti[day].open && !this.orariAppuntamenti[day + '2'].open && this.orariAppuntamenti[day].close && !this.orariAppuntamenti[day + '2'].close) {
        // ci son eventi di mattina ma non pomeriggio
        this.attivaPicker(this.orariAppuntamenti[day].open, this.orariAppuntamenti[day].close, '', '', dis);
      // tslint:disable-next-line:max-line-length
      } else if (!this.orariAppuntamenti[day].open && this.orariAppuntamenti[day + '2'].open && !this.orariAppuntamenti[day].close && this.orariAppuntamenti[day + '2'].close) {
        // ci son eventi di pomeriggio ma non di mattina
        this.attivaPicker(this.orariAppuntamenti[day + '2'].open, this.orariAppuntamenti[day + '2'].close, '', '', dis);
      // tslint:disable-next-line:max-line-length
      } else if (this.orariAppuntamenti[day].open && this.orariAppuntamenti[day + '2'].open && this.orariAppuntamenti[day].close && this.orariAppuntamenti[day + '2'].close) {
        // ci son eventi sia di mattina che di pomeriggio
        // tslint:disable-next-line:max-line-length
        this.attivaPicker(this.orariAppuntamenti[day].open, this.orariAppuntamenti[day].close, this.orariAppuntamenti[day + '2'].open, this.orariAppuntamenti[day + '2'].close, dis);
      }
    });
  }
  attivaPicker (open: string, close: string, open2: string, close2: string, disabled: string[]) {
    try {
      const myNode = document.getElementById('appo-picker-main');
      myNode.remove();
    } catch (err) {console.log(err); }
    this.picker = new appPicker(document.getElementById('time-2'), {
      interval: this.dataSer.medico.durataAppuntamenti,
      mode: '24h',
      startTime: open,
      endTime: close,
      startTime2: open2,
      endTime2: close2,
      disabled: disabled,
      large: this.isLargeOrariPicker
      // gestisci responsive
    });
    // cdr detect change a orari appuntamenti
    const cssRules = '.appo-picker {position: absolute;' +
                  '   display: none;' +
                  '   background-color: #F5F5F5;' +
                  '   width: calc(100% - 20px);' + // vero width complessivo
                  '   padding: 40px;' +
                  '   border: 1px solid #ccc;' +
                  '   box-shadow: 0 5px 15px -5px #ccc;' +
                  '   z-index: 9999;' +
                  '   font-family: Helvetica, Arial, sans-serif;' +
                  '}' +
                  '.appo-picker.is-open {' +
                  '   display: inline-block;' +
                  '   /* IE 9 */' +
                  '   display: -ms-flexbox;' +
                  '   /* IE 10 */' +
                  '   display: -webkit-flex;' +
                  '   /* Safari 6.1+. iOS 7.1+, BB10 */' +
                  '   display: flex;' +
                  '  left: -25px !important;' +
                  '  width: calc(100% + 40px);' +
                  '  padding: 10px;' +
                  '   -ms-flex-direction: column;' +
                  '   -webkit-flex-direction: column;' +
                  '   flex-direction: column; ' +
                  '}' +
                  '.appo-picker.is-position-static {' +
                  '  display: flex;' +
                  '  flex-direction: column;' +
                  '  position: static; }' +
                  '.appo-picker.is-large {' + // is large sta QUAAA
                  '  width: calc(100% + 115px);' +
                  '  padding: 40px;' +
                  '  left: -160px !important;' + // quA LO SPOSTO A SINISTRA COSI è BELLO
                  '  box-shadow: 0 5px 15px 0 #ccc; }' +
                  '  .appo-picker.is-large .appo-picker-title {' +
                  '    height: 40px;' +
                  '    font-size: 18px; }' +
                  '  .appo-picker.is-large .appo-picker-list {' +
                  '    max-width: 100%; }' +
                  '  .appo-picker.is-large .appo-picker-list-item {' +
                  '    width: 25%;' +
                  '    height: 47px;' +
                  '    font-size: 16px; }' +
                  '.appo-picker-title {' +
                  '  display: -ms-flexbox;' +
                  '  display: -webkit-flex;' +
                  '  display: flex;' +
                  '  -webkit-justify-content: center;' +
                  '  -ms-flex-pack: center;' +
                  '  justify-content: center;' +
                  '  height: 30px;' +
                  '  font-size: 14px;' +
                  '  font-weight: bold; }' +
                  '.appo-picker-list {' +
                  '  display: -ms-flexbox;' +
                  '  display: -webkit-flex;' +
                  '  display: flex;' +
                  '  -ms-flex-direction: row;' +
                  '  -webkit-flex-direction: row;' +
                  '  flex-direction: row;' +
                  '  -ms-flex-wrap: wrap;' +
                  '  -webkit-flex-wrap: wrap;' +
                  '  flex-wrap: wrap;' +
                  '  max-width: 100%;' +
                  '  padding: 0;' +
                  '  margin: 0;' +
                  '  list-style-type: none; }' +
                  '.appo-picker-list-item {' +
                  '  display: inline-block;' +
                  '  /* IE 9 */' +
                  '  display: flex;' +
                  '  width: 25%;' +
                  '  height: 54px; }' + // vera height
                  '  .appo-picker-list-item input[type="button"] {' +
                  '    width: 100%;' +
                  '    height: 100%;' +
                  '    margin: 0 1px 1px 0;' +
                  '    border-color: #F5F5F5;' +
                  '    border-style: solid;' +
                  '    border-width: 1px 1px 0 0;' +
                  '    border-radius: 5px;' +
                  '    background-color: #f5f5f5;' +
                  '    font-size: 16px;' + // vero font
                  '    color: #666;' +
                  '    line-height: 15px;' +
                  '    -webkit-appearance: none;' +
                  '    -moz-appearance: none;' +
                  '    cursor: pointer; }' +
                  '    .appo-picker-list-item input[type="button"].is-selected {' +
                  '      background-color: #33aaff;' +
                  '      color: #F5F5F5;' +
                  '      box-shadow: inset 0 1px 2px #666; }' +
                  '    .appo-picker-list-item input[type="button"]:hover,' +
                  '    .appo-picker-list-item input[type="button"]:active, ' +
                  '    .appo-picker-list-item input[type="button"]:focus {' +
                  '      color: #F5F5F5;' +
                  '      background-color: #ff8000;' +
                  '      outline: none; }' +
                  '    .appo-picker-list-item input[type="button"]:disabled {' +
                  '      background-color: #D5E9F7;' +
                  '      opacity: 0.3;' +
                  '      color: #666;' +
                  '      cursor: auto;' +
                  '}';
    const css = document.createElement('style');
    css.type = 'text/css';
    css.innerHTML = cssRules;
    document.body.appendChild(css);
    this.disattivaPicker = false;
  }
  getOrarioSede(idMed: string): Observable<any> {
    const orariDoc = this.DB.getDocument(`medici/${idMed}/sede/${this.dataSer.userData.sedeRef}/orari/orari`);
    this.orari$ = orariDoc.valueChanges().pipe(
      filter(val => val != null),
      map(val => {
        this.orari = val.orari;
        this.orariAppuntamenti = val.orariAppuntamenti;
        return val;
      })
    );
    return this.orari$;
  }
  getEventiSede(idMed: string, fromCal: boolean = true, field = 'inizio', modOrdine: OrderByDirection = 'desc') {
    const eventiDoc = this.DB.getOrderedCollection(`medici/${idMed}/sede/${this.dataSer.userData.sedeRef}/appuntamenti`, field, modOrdine);
    const eventi$ = eventiDoc.snapshotChanges().pipe(
      map(arr => {
        // ogni volta che aggiungi un evento que si update
        this.eventi = [];
        return this.formatEvent(arr.map(snap => {
          const data = snap.payload.doc.data();
          const id = snap.payload.doc.id;
          if (data.inizio) {
            this.eventi.push({ id, ...data });
          }
          return { id, ...data };
        }), fromCal);
      })
    );
    return eventi$;
  }
  formatEvent(events: EventDB [], fromCal: boolean = true): CalendarEvent[] {
    this.events = [];
    if (fromCal) { this.riempiIndisponibilita(); }
    for (const item in events) {
      if (events[item].inizio && events[item].userRef && events[item].id) {
        this.events.push({
          'start': new Date(events[item].inizio.toMillis()),
          'end': new Date(events[item].inizio.toMillis() + this.dataSer.medico.durataAppuntamenti * 60000),
          'title': '', // events[item].userRef,
          'color': (events[item].confermato === 'true' ? this.colors.red : this.colors.blue),
          'id'   : events[item].id
        });
      }
    }
    this.refresh.next();
    return this.events;
  }
  setDurataAppuntamenti(event) {
    this.dataSer.medico.durataAppuntamenti = event.target.value;
    this.DB.updateDocument(`medici/${this.dataSer.medico.id}`, {durataAppuntamenti: event.target.value});
  }
  deleteEvento(id: string, numEvento: string) {
    this.DB.delDocument(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/appuntamenti/${id}`);
    this.events[numEvento] = {};
    this.dataSer.openSnackBar('Evento eliminato', '');
  }
  confermaEvento(id: string, numEvento: string) {
    this.DB.updateDocument(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/appuntamenti/${id}`, {confermato: 'true'});
    if (numEvento) {
      this.events[numEvento].confermato = true;
    }
  }
  riempiIndisponibilita() { // riempie il giorno della settimana con evento indisponibilità
    const viewDayOfWeek = this.viewDate.getDay();
    let viewDate$: Date = this.viewDate;
    let day = viewDayOfWeek;
    while (day >= 0) {
      switch (day) {
        case 1: this.riempiIndisponibilitaDay(viewDate$, 'lun'); break;
        case 2: this.riempiIndisponibilitaDay(viewDate$, 'mar'); break;
        case 3: this.riempiIndisponibilitaDay(viewDate$, 'mer'); break;
        case 4: this.riempiIndisponibilitaDay(viewDate$, 'gio'); break;
        case 5: this.riempiIndisponibilitaDay(viewDate$, 'ven'); break;
        case 6: this.riempiIndisponibilitaDay(viewDate$, 'sab'); break;
        case 0: this.riempiIndisponibilitaDay(viewDate$, 'dom'); break;
      }
      // viewDate$.setDate(viewDate$.getDate() - 1);
      viewDate$ = new Date(viewDate$.getTime() - 86400000); // 86400000 ms in 1 day
      day --;
    }
    day = viewDayOfWeek + 1;
    viewDate$ = new Date(this.viewDate.getTime() + 86400000);
    while (day <= 6) {
      switch (day) {
        case 1: this.riempiIndisponibilitaDay(viewDate$, 'lun'); break;
        case 2: this.riempiIndisponibilitaDay(viewDate$, 'mar'); break;
        case 3: this.riempiIndisponibilitaDay(viewDate$, 'mer'); break;
        case 4: this.riempiIndisponibilitaDay(viewDate$, 'gio'); break;
        case 5: this.riempiIndisponibilitaDay(viewDate$, 'ven'); break;
        case 6: this.riempiIndisponibilitaDay(viewDate$, 'sab'); break;
        case 0: this.riempiIndisponibilitaDay(viewDate$, 'dom'); break;
      }
      viewDate$ = new Date(viewDate$.getTime() + 86400000); // 86400000 ms in 1 day
      // viewDate$.setDate(viewDate$.getDate() + 1);
      day ++;
    }
  }
  deleteIndisponibilita() {
    for (const event in this.events) {
      if (this.events[event].cssClass === 'nondisponibile') {
        this.events[event] = '';
      }
    }
  }
  riempiIndisponibilitaDay(data: Date, day: string) {
    // chiama questa funzione quando cambi l'orario cosi non vengono rimessi tutti gli orari ma solo quello cambiato
    let newEvent;
    // tslint:disable-next-line:max-line-length
    if (!this.orariAppuntamenti[day].open && !this.orariAppuntamenti[day + '2'].open && !this.orariAppuntamenti[day].close && !this.orariAppuntamenti[day + '2'].close) {
      // non ci son eventi ne di mattina ne pom
      newEvent = {
        'start': new Date(data.setHours(0, 0)),
        'end': new Date(data.setHours(23, 59, 59, 999)),
        'title': '',
        'color': this.colors.disabled,
        'cssClass': 'nondisponibile'
      };
      this.events.push(newEvent);
    // tslint:disable-next-line:max-line-length
    } else if (this.orariAppuntamenti[day].open && !this.orariAppuntamenti[day + '2'].open && this.orariAppuntamenti[day].close && !this.orariAppuntamenti[day + '2'].close) {
      // ci son eventi di mattina ma non pomeriggio
      newEvent = {
        'start': new Date(data.setHours(0, 0)),
        // tslint:disable-next-line:max-line-length
        'end': new Date(data.setHours(Number(this.orariAppuntamenti[day].open.substr(0, 2)), Number(this.orariAppuntamenti[day].open.substr(3, 2)))),
        'title': '',
        'color': this.colors.disabled,
        'cssClass': 'nondisponibile'
      };
      this.events.push(newEvent);
      newEvent = {
        // tslint:disable-next-line:max-line-length
        'start': new Date(data.setHours(Number(this.orariAppuntamenti[day].close.substr(0, 2)), Number(this.orariAppuntamenti[day].close.substr(3, 2)))),
        'end': new Date(data.setHours(23, 59, 59, 999)),
        'title': '',
        'color': this.colors.disabled,
        'cssClass': 'nondisponibile'
      };
      this.events.push(newEvent);
    // tslint:disable-next-line:max-line-length
    } else if (!this.orariAppuntamenti[day].open && this.orariAppuntamenti[day + '2'].open && !this.orariAppuntamenti[day].close && this.orariAppuntamenti[day + '2'].close) {
      // ci son eventi di pomeriggio ma non di mattina
      newEvent = {
        'start': new Date(data.setHours(0, 0)),
        // tslint:disable-next-line:max-line-length
        'end': new Date(data.setHours(Number(this.orariAppuntamenti[day + '2'].open.substr(0, 2)), Number(this.orariAppuntamenti[day + '2'].open.substr(3, 2)))),
        'title': '',
        'color': this.colors.disabled,
        'cssClass': 'nondisponibile'
      };
      this.events.push(newEvent);
      newEvent = {
        // tslint:disable-next-line:max-line-length
        'start': new Date(data.setHours(Number(this.orariAppuntamenti[day + '2'].close.substr(0, 2)), Number(this.orariAppuntamenti[day + '2'].close.substr(3, 2)))),
        'end': new Date(data.setHours(23, 59, 59, 999)),
        'title': '',
        'color': this.colors.disabled,
        'cssClass': 'nondisponibile'
      };
      this.events.push(newEvent);
    // tslint:disable-next-line:max-line-length
    } else if (this.orariAppuntamenti[day].open && this.orariAppuntamenti[day + '2'].open && this.orariAppuntamenti[day].close && this.orariAppuntamenti[day + '2'].close) {
      // ci son eventi sia di mattina che di pomeriggio
      newEvent = {
        'start': new Date(data.setHours(0, 0)),
        // tslint:disable-next-line:max-line-length
        'end': new Date(data.setHours(Number(this.orariAppuntamenti[day].open.substr(0, 2)), Number(this.orariAppuntamenti[day].open.substr(3, 2)))),
        'title': '',
        'color': this.colors.disabled,
        'cssClass': 'nondisponibile'
      };
      this.events.push(newEvent);
      newEvent = {
        // tslint:disable-next-line:max-line-length
        'start': new Date(data.setHours(Number(this.orariAppuntamenti[day].close.substr(0, 2)), Number(this.orariAppuntamenti[day].close.substr(3, 2)))),
        // tslint:disable-next-line:max-line-length
        'end': new Date(data.setHours(Number(this.orariAppuntamenti[day + '2'].open.substr(0, 2)), Number(this.orariAppuntamenti[day + '2'].open.substr(3, 2)))),
        'title': '',
        'color': this.colors.disabled,
        'cssClass': 'nondisponibile'
      };
      this.events.push(newEvent);
      newEvent = {
        // tslint:disable-next-line:max-line-length
        'start': new Date(data.setHours(Number(this.orariAppuntamenti[day + '2'].close.substr(0, 2)), Number(this.orariAppuntamenti[day + '2'].close.substr(3, 2)))),
        'end': new Date(data.setHours(23, 59, 59, 999)),
        'title': '',
        'color': this.colors.disabled,
        'cssClass': 'nondisponibile'
      };
      this.events.push(newEvent);
    }
    this.refresh.next();
    // data.setHours(0,0);
  }
  changeViewDate(action: string) {
    if (action === 'back') {
      this.viewDate = new Date(this.viewDate.getTime() - 7 * 24 * 60 * 60000); // togli una settimana all viewDate
    } else if (action === 'today') {
      this.viewDate = new Date();                                              // riporta la viewDate a oggi
    } else if (action === 'forward') {
      this.viewDate = new Date(this.viewDate.getTime() + 7 * 24 * 60 * 60000); // aggiungi una settimana all viewDate
    }
    this.riempiIndisponibilita();
    // this.refresh.next(); non serve
  }
  startClickToCreate(segment: DayViewHourSegment, mouseDownEvent: MouseEvent, segmentElement: HTMLElement) {
    const calendarioHTML = document.getElementById('calendarioAppuntamenti');
    fromEvent(calendarioHTML, 'click')
      .pipe(
        take(1),
        filter(val => val != null)
      )
      .subscribe((mouseMoveEvent: MouseEvent) => {
        const dateEvent = new Date(segment.date.getTime() + mouseMoveEvent.layerY * 60000);
        const blocco    = this.trovaBloccoOrario(dateEvent); // ritorna il blocco giusto, tipo: 'mar2', 'ven'...
        // tslint:disable-next-line:max-line-length
        const dateOpen  = new Date(dateEvent.getFullYear(), dateEvent.getMonth(), dateEvent.getDate(), this.orariAppuntamenti[blocco].open.substr(0, 2), this.orariAppuntamenti[blocco].open.substr(3, 2) , 0, 0);
        // tslint:disable-next-line:max-line-length
        const dateClose = new Date(dateEvent.getFullYear(), dateEvent.getMonth(), dateEvent.getDate(), this.orariAppuntamenti[blocco].close.substr(0, 2), this.orariAppuntamenti[blocco].close.substr(3, 2) , 0, 0);
        let finalDateOpen   = dateOpen;
        let finalDateClose   = new Date(dateOpen.getTime() + this.dataSer.medico.durataAppuntamenti * 60000);
        const minutiBlocco = this.getMinBlocco(dateClose, dateOpen); // ritorna durata del blocco in number
        const numAppuntamentiMax = minutiBlocco / this.dataSer.medico.durataAppuntamenti;
        for (let i = 0; i < numAppuntamentiMax; i++) { // for per dividere in blocchi in base a durata appuntamenti e trovare blocco giusto
          if (finalDateClose.getTime() < dateEvent.getTime()) {
            // tslint:disable-next-line:max-line-length
            finalDateOpen = new Date(finalDateOpen.getTime() + this.dataSer.medico.durataAppuntamenti * 60000); // aggiunto durataAppuntamenti in minuti a data
            // tslint:disable-next-line:max-line-length
            finalDateClose = new Date(finalDateClose.getTime() + this.dataSer.medico.durataAppuntamenti * 60000); // aggiunto durataAppuntamenti in minuti a data
          }
        }
        // impedire overlapping con indisponibilità
        if (finalDateClose.getHours().toString() + ':' + finalDateClose.getMinutes().toString() < this.orariAppuntamenti[blocco].close) {
          const dragToSelectEvent: CalendarEvent = {
            id: this.events.length,
            title: 'New event',
            start: finalDateOpen,
            end: finalDateClose,
            confermato: false
          };
          // tslint:disable-next-line:max-line-length
          this.DB.addToCollectionWithName(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/appuntamenti`,     // metti dalla parte dell'user
          // tslint:disable-next-line:max-line-length
          {inizio: new Date(dragToSelectEvent.start), userRef: {id: `${this.dataSer.medico.id}${new Date()}`}, nominativo: 'Paziente', confermato: 'true', data: new Date(dragToSelectEvent.start).setHours(0, 0, 0, 0), messaggio: 'prenotato da medico', fromMedico: true}, `app${this.dataSer.medico.id}${new Date()}` ); // con la corretta userRef
          this.events = [...this.events, dragToSelectEvent];
          // per eliminare l'evento si passa l'id che sarebbe l'userRef.id preceduto da 'app'
          // quindi creo il documento con nome univoco preceduto da 'app'
        }
      });
  }
  trovaBloccoOrario(date: Date): string {
    const dayOfWeek = date.getDay(); // valore in number del giorno settimana
    let blocco = ''; // lun,lun2...
    let dayStringOfWeek = ''; // lun,mar,mer per trovare il blocco
    const orarioDate = date.toString().substr(16, 5); // HH:ii per trovare blocco
    switch (dayOfWeek) {
      case 0:
        dayStringOfWeek = 'dom';
      break;
      case 1:
        dayStringOfWeek = 'lun';
      break;
      case 2:
        dayStringOfWeek = 'mar';
      break;
      case 3:
        dayStringOfWeek = 'mer';
      break;
      case 4:
        dayStringOfWeek = 'gio';
      break;
      case 5:
        dayStringOfWeek = 'ven';
      break;
      case 6:
        dayStringOfWeek = 'sab';
      break;
    }

    if (orarioDate < this.orariAppuntamenti[dayStringOfWeek].close && orarioDate > this.orariAppuntamenti[dayStringOfWeek].open) {
      blocco = dayStringOfWeek;
    // tslint:disable-next-line:max-line-length
    } else if (orarioDate < this.orariAppuntamenti[dayStringOfWeek + '2'].close && orarioDate > this.orariAppuntamenti[dayStringOfWeek + '2'].open) {
      blocco = dayStringOfWeek + '2';
    }
    return blocco;
  }
  floorToNearest(amount: number, precision: number) {
    return Math.floor(amount / precision) * precision;
  }
  getMinBlocco(dateClose: Date, dateOpen: Date): number {
    const differenceMs = dateClose.getTime() - dateOpen.getTime();
    const differenceMin = (differenceMs / 1000) / 60;
    return differenceMin;
  }
  checkOverlappingAMPM(str: string, open_close: string, tipo: string) {
    let altro = ''; // se str = lun  ==>  altro = lun2
    let orari;
    if (tipo === 'attivita') {
      orari = this.orari;
    } else if (tipo === 'appuntamenti') {
      orari = this.orariAppuntamenti;
    }
    if (open_close === 'open') {
      if (str.length === 4) {
        altro = str.substring(0, str.length - 1);
        // tslint:disable-next-line:max-line-length
        if (orari[str].open < orari[altro].close && orari[str].open >= orari[altro].open && orari[altro].close !== '' && orari[altro].open !== '' && orari[str].open !== '') {
          // orari[str].open = orari[altro].close;
          orari[str].open = '';
          orari[str].close = '';
          this.dataSer.openSnackBar('collisione orari AM PM', '');
        } else if (orari[str].open < orari[altro].open && orari[altro].close !== '' && orari[str].open !== '') {
          orari[str].open = '';
          orari[str].close = '';
          this.dataSer.openSnackBar('AM dopo PM', '');
        }
      } else if (str.length === 3) {
        altro = str + '2';
        // tslint:disable-next-line:max-line-length
        if (orari[str].open < orari[altro].close && orari[str].close > orari[altro].open && orari[str].open <= orari[altro].open && orari[altro].close !== '' && orari[altro].open !== '' && orari[str].open !== '') {
          orari[str].open = '';
          orari[str].close = '';
          this.dataSer.openSnackBar('collisione orari AM PM', '');
        } else if (orari[str].open > orari[altro].open && orari[str].close !== '' && orari[altro].open !== '') {
          orari[str].open = '';
          orari[str].close = '';
          this.dataSer.openSnackBar('AM dopo PM', '');
        }
      }
    } else if (open_close === 'close') {
      if (str.length === 4) { // PM
        altro = str.substring(0, str.length - 1);
        // tslint:disable-next-line:max-line-length
        if (orari[str].close > orari[altro].open && orari[str].open < orari[altro].close && orari[str].close >= orari[altro].close && orari[altro].close !== '' && orari[altro].open !== '' && orari[str].close !== '') {
          orari[str].open = '';
          orari[str].close = '';
          this.dataSer.openSnackBar('collisione orari AM PM', '');
        } else if (orari[str].close < orari[altro].close && orari[str].close !== '' && orari[altro].close !== '') {
          orari[str].open = '';
          orari[str].close = '';
          this.dataSer.openSnackBar('AM dopo PM', '');
        }
      } else if (str.length === 3) { // AM
        altro = str + '2';
        // tslint:disable-next-line:max-line-length
        if (orari[str].close > orari[altro].open && orari[str].close <= orari[altro].close && orari[altro].close !== '' && orari[altro].open !== '' && orari[str].close !== '') {
          // orari[str].close = orari[altro].open;
          orari[str].open = '';
          orari[str].close = '';
          this.dataSer.openSnackBar('collisione orari AM PM', '');
        } else if (orari[str].close > orari[altro].close && orari[str].close !== '' && orari[altro].close !== '') {
          orari[str].open = '';
          orari[str].close = '';
          this.dataSer.openSnackBar('AM dopo PM', '');
        }
      }
    }
    if (tipo === 'attivita') {
      this.orari = orari;
    } else if (tipo === 'appuntamenti') {
      this.orariAppuntamenti = orari;
    }
  }
  cambiaOrarioApertura(e, str: string, tipo: string = 'attivita') {
    this.cambiaAttrOrari(str, 'open', e.target.value, tipo);
    const data = this.dinamicObjGiornoSett(str, 'open', e.target.value, tipo);
    this.checkOverlappingAMPM(str, 'open', tipo);
    if (data.err) {
      this.dataSer.openSnackBar('errore', '');
    }
    if (tipo === 'attivita') {
      this.DB.updateDocument(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/orari/orari`, {orari: this.orari});
    } else if (tipo === 'appuntamenti') {
      this.deleteIndisponibilita();
      this.riempiIndisponibilita();
      // tslint:disable-next-line:max-line-length
      this.DB.updateDocument(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/orari/orari`, {orariAppuntamenti: this.orariAppuntamenti});
    }
  }
  cambiaOrarioChiusura(e, str: string, tipo: string = 'attivita') {
    this.cambiaAttrOrari(str, 'close', e.target.value, tipo);
    const data = this.dinamicObjGiornoSett(str, 'close', e.target.value, tipo);
    this.checkOverlappingAMPM(str, 'close', tipo);
    if (data.err) {
      this.dataSer.openSnackBar('errore', '');
    }
    if (tipo === 'attivita') {
      this.DB.updateDocument(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/orari/orari`, {orari: this.orari});
    } else if (tipo === 'appuntamenti') {
      this.deleteIndisponibilita();
      this.riempiIndisponibilita();
      // tslint:disable-next-line:max-line-length
      this.DB.updateDocument(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/orari/orari`, {orariAppuntamenti: this.orariAppuntamenti});
    }
  }
  calcolaInizioCal(): number {
    let minimo = '23:59';
    for (const key in this.orari) {
      if (Object.prototype.hasOwnProperty.call(this.orari, key)) {
        if (minimo > this.orari[key].open && this.orari[key].open > '00:00') { minimo = this.orari[key].open; }
      }
    }
    for (const key in this.orariAppuntamenti) {
      if (Object.prototype.hasOwnProperty.call(this.orariAppuntamenti, key)) {
        if (minimo > this.orariAppuntamenti[key].open  && this.orariAppuntamenti[key].open > '00:00') {
          minimo = this.orariAppuntamenti[key].open;
        }
      }
    }
    return Math.floor(Number(minimo.replace(':', '.')));
  }
  calcolaFineCal(): number {
    let massimo = '00:00';
    /*for (const key in this.calSer.orari) {
      if (Object.prototype.hasOwnProperty.call(this.calSer.orari, key)) {
        if (massimo < this.calSer.orari[key].close) { massimo = this.orari[key].close; }
      }
    }*/
    for (const key in this.orariAppuntamenti) {
      if (Object.prototype.hasOwnProperty.call(this.orariAppuntamenti, key)) {
        if (massimo < this.orariAppuntamenti[key].close) { massimo = this.orariAppuntamenti[key].close; }
      }
    }
    let toReturn = Math.ceil(Number(massimo.replace(':', '.')));
    if (toReturn > 23.59) { toReturn = 23.59; }
    return toReturn;
  }
  dinamicObjGiornoSett(day: string, open_close: string, val: string, tipo: string): any {
    // ritorna oggetto lun-mar-mer2... e reimposta orari se sballati
    let data = {};
    let orari;
    let newval;
    if (tipo === 'appuntamenti') {
      orari = this.orariAppuntamenti;
    } else if (tipo === 'attivita') {
      orari = this.orari;
    }
    if (open_close === 'open') { // caso normale
      if (val < orari[day].close && val !== '' && orari[day].close !== '') {
        data = {};
      } else { // errore input utente
        if (tipo === 'appuntamenti') { // appuntamenti
          if (orari[day].close === '' && val !== '') { // immissione apertura con chiusura null
            let val2 = Number(orari[day].open.substring(0, 2)) + 1 + '';
            if (val2.length === 1) { val2 = '0' + val2; } if (val2 < '00') { val2 = '00'; } if (val2 > '23:59') { val2 = '23:59'; }
            if (val2.length === 2) { val2 = val2 + ':00'; }
            data = {};
            this.orariAppuntamenti[day].close = val2;
          } else if (val === '') { // campo modificato portato a ''
            this.orariAppuntamenti[day].close = '';
          } else { // errore input utente close < open
            newval = Number(this.orariAppuntamenti[day].close.substring(0, 2)) - 1 + '';
            if (newval.length === 1) { newval = '0' + newval; }
            if (newval < '00') { newval = '00'; } if (newval > '23:59') { newval = '23:59'; }
            if (newval.length === 2) { newval = newval + ':00'; }
            data = { err: true};
            this.orariAppuntamenti[day].open = newval;
          }
        } else if (tipo === 'attivita') { // attivita
          if (orari[day].close === '' && val !== '') { // immissione apertura con chiusura null
            let val2 = Number(orari[day].open.substring(0, 2)) + 1 + '';
            if (val2.length === 1) { val2 = '0' + val2; } if (val2 < '00') { val2 = '00'; } if (val2 > '23:59') { val2 = '23:59'; }
            if (val2.length === 2) { val2 = val2 + ':00'; }
            data = {};
            this.orari[day].close = val2;
            console.log(this.orari);
          } else if (val === '') { // campo modificato portato a ''
            this.orari[day].close = '';
          } else { // errore input utente close < open
            newval = Number(this.orari[day].close.substring(0, 2)) - 1 + '';
            if (newval.length === 1) { newval = '0' + newval; }
            if (newval < '00') { newval = '00'; } if (newval > '23:59') { newval = '23:59'; }
            if (newval.length === 2) { newval = newval + ':00'; }
            data = { err: true };
            this.orari[day].open = newval;
          }
        }
      }
    } else if (open_close === 'close') {
      if (val > orari[day].open && val !== '' && orari[day].open !== '') {
        data = {};
      } else {
        if (tipo === 'appuntamenti') {
          if (orari[day].open === '' && val !== '') { // immissione chiusura con apertura null
            let val2 = Number(orari[day].close.substring(0, 2)) - 1 + '';
            if (val2.length === 1) { val2 = '0' + val2; } if (val2 < '00') { val2 = '00'; } if (val2 > '23:59') { val2 = '23:59'; }
            if (val2.length === 2) { val2 = val2 + ':00'; }
            data = {};
            this.orariAppuntamenti[day].open = val2;
          } else if (val === '') { // campo modificato portato a ''
            this.orariAppuntamenti[day].open = '';
          } else { // errore input utente close < open
            newval = Number(this.orariAppuntamenti[day].open.substring(0, 2)) + 1 + '';
            if (newval.length === 1) { newval = '0' + newval; }
            if (newval < '00') { newval = '00'; } if (newval > '23:59') { newval = '23:59'; }
            if (newval.length === 2) { newval = newval + ':00'; }
            data = { err: true};
            this.orariAppuntamenti[day].close = newval;
          }
        } else if (tipo === 'attivita') {
          if (orari[day].open === '' && val !== '') { // immissione chiusura con apertura null
            let val2 = Number(orari[day].close.substring(0, 2)) - 1 + '';
            if (val2.length === 1) { val2 = '0' + val2; } if (val2 < '00') { val2 = '00'; } if (val2 > '23:59') { val2 = '23:59'; }
            if (val2.length === 2) { val2 = val2 + ':00'; }
            data = {};
            this.orari[day].open = val2;
          } else if (val === '') { // campo modificato portato a ''
            this.orari[day].open = '';
          } else { // errore input utente close < open
            newval = Number(this.orari[day].open.substring(0, 2)) + 1 + '';
            if (newval.length === 1) { newval = '0' + newval; }
            if (newval < '00') { newval = '00'; } if (newval > '23:59') { newval = '23:59'; }
            if (newval.length === 2) { newval = newval + ':00'; }
            data = { err: true};
            this.orari[day].close = newval;
          }
        }
      }
    }
    return data;
  }
  azzeraOrario(tipo: string) {
    const data1 = {
      orari: {
        'lun' : { open : '', close: ''},
        'mar' : { open : '', close: ''},
        'mer' : { open : '', close: ''},
        'gio' : { open : '', close: ''},
        'ven' : { open : '', close: ''},
        'sab' : { open : '', close: ''},
        'dom' : { open : '', close: ''},
        'lun2': { open : '', close: ''},
        'mar2': { open : '', close: ''},
        'mer2': { open : '', close: ''},
        'gio2': { open : '', close: ''},
        'ven2': { open : '', close: ''},
        'sab2': { open : '', close: ''},
        'dom2': { open : '', close: ''}
      }
    };
    const data2 = {
      orariAppuntamenti: {
        'lun' : { open : '', close: ''},
        'mar' : { open : '', close: ''},
        'mer' : { open : '', close: ''},
        'gio' : { open : '', close: ''},
        'ven' : { open : '', close: ''},
        'sab' : { open : '', close: ''},
        'dom' : { open : '', close: ''},
        'lun2': { open : '', close: ''},
        'mar2': { open : '', close: ''},
        'mer2': { open : '', close: ''},
        'gio2': { open : '', close: ''},
        'ven2': { open : '', close: ''},
        'sab2': { open : '', close: ''},
        'dom2': { open : '', close: ''}
      }
    };
    if (tipo === 'attivita') {
      this.DB.updateDocument(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/orari/orari`, data1);
      // tslint:disable-next-line:forin
      for (const item in this.orari) {
        this.orari[item] = { open : '', close: ''};
      }
    } else if (tipo === 'appuntamenti') {
      this.DB.updateDocument(`medici/${this.dataSer.medico.id}/sede/${this.dataSer.medico.sedeRef}/orari/orari`, data2);
      // tslint:disable-next-line:forin
      for (const item in this.orariAppuntamenti) {
        this.orariAppuntamenti[item] = { open : '', close: ''};
      }
    }
  }
  cambiaAttrOrari(day: string, open_close: string, val: string, tipo: string ) { // cambia un attributo alla volta negli orari
    let orari;
    if (tipo === 'appuntamenti') {
      orari = this.orariAppuntamenti;
    } else if (tipo === 'attivita') {
      orari = this.orari;
    }
    if (open_close === 'open')  { orari[day].open = val ;  } else if (open_close === 'close') { orari[day].close = val ;  }
    if (tipo === 'appuntamenti') {
      this.orariAppuntamenti = orari;
    } else if (tipo === 'attivita') {
      this.orari = orari;
    }
  }
}
