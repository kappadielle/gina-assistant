import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrSettingsComponent } from './dr-settings.component';

describe('DrSettingsComponent', () => {
  let component: DrSettingsComponent;
  let fixture: ComponentFixture<DrSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
