import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DBmanagerService } from '../dbmanager.service';
import { Medico } from '../medico';
import { AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable, Subscription } from 'rxjs';
import { filter, take } from 'rxjs/operators';
import { UserDataService } from '../user-data.service';
import { CalendarioService } from '../calendario.service';

@Component({
  selector: 'app-dr-settings',
  templateUrl: './dr-settings.component.html',
  styleUrls: ['./dr-settings.component.css'],
})

export class DrSettingsComponent implements OnInit {
  medDoc: AngularFirestoreDocument<Medico>;
  medObs: Observable<Medico>;
  subMed: Subscription;
  subUserData: Subscription;
  medico: Medico;
  modSala: string;
  // tslint:disable-next-line:max-line-length
  constructor(private DB: DBmanagerService, private dataSer: UserDataService, private CDR: ChangeDetectorRef, public calSer: CalendarioService) { }
  ngOnInit() {
    if (this.dataSer.userData) {
      this.medico = this.dataSer.userData;
      if (!this.medico.orarioRicette) { this.medico.orarioRicette = ''; }
      if (!this.medico.durataAppuntamenti) { this.medico.durataAppuntamenti = 20; }
      this.modSala = this.dataSer.modSala;
    } else {
      this.subUserData = this.dataSer.userData$.pipe(
        filter(val => val != null), take(1)
      )
      .subscribe(user => {
        this.medico = user;
        if (!this.medico.orarioRicette) { this.medico.orarioRicette = ''; }
        if (!this.medico.durataAppuntamenti) { this.medico.durataAppuntamenti = 20; }
        this.modSala = this.dataSer.modSala;
      });
    }
    this.CDR.detectChanges();
  }
  setOrarioRiconsegnaRicette($event) {
    this.medico.orarioRicette = $event.target.value;
    this.DB.updateDocument(`medici/${this.medico.id}`, {orarioRicette: $event.target.value});
  }
  setRicetteStatiche($event) {
    this.medico.ricetteStatiche = $event.checked;
    this.DB.updateDocument(`medici/${this.medico.id}`, {ricetteStatiche: $event.checked});
  }
}
