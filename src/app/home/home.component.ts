import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../user-data.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { User } from '../user';
import { trigger, state, style, transition, animate, query, stagger } from '@angular/animations';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('slide1', [
      state('true', style({
        marginLeft: '0'
      })),
      transition('false => true', [
        animate('600ms cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
    trigger('img1', [
      state('true', style({
        opacity: '1'
      })),
      transition('false => true', [
        animate('600ms cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
    trigger('slide2', [
      state('true', style({
        marginLeft: '20%'
      })),
      transition('false => true', [
        animate('600ms cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
    trigger('img2', [
      state('true', style({
        opacity: '1'
      })),
      transition('false => true', [
        animate('600ms cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
    trigger('slide3', [
      state('true', style({
        marginLeft: '0'
      })),
      transition('false => true', [
        animate('600ms cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
    trigger('img3', [
      state('true', style({
        opacity: '1'
      })),
      transition('false => true', [
        animate('600ms cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
    trigger('img4', [
      state('true', style({
        opacity: '1'
      })),
      transition('false => true', [
        animate('600ms cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
    trigger('sgobbo1', [
      state('true', style({
        opacity: '1'
      })),
      transition('false => true', [
        animate('600ms cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
    trigger('sgobbo2', [
      state('true', style({
        opacity: '1'
      })),
      transition('false => true', [
        animate('600ms cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
    trigger('sgobbo3', [
      state('true', style({
        opacity: '1'
      })),
      transition('false => true', [
        animate('600ms cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
    trigger('sgobbo4', [
      state('true', style({
        opacity: '1'
      })),
      transition('false => true', [
        animate('600ms cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
    trigger('text', [
      state('true', style({
        opacity: '1'
      })),
      transition('false => true', [
        animate('600ms cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
  ]
})
export class HomeComponent implements OnInit {
  medico: any;
  subUserData: Subscription;
  emailDaVerificare: string;
  myStorage: any;
  currentUser: User;
  constructor(private dataSer: UserDataService, private router: Router) { }
  animations: any;
  // tslint:disable-next-line:quotemark
  ngOnInit() {
    this.animations = {
      'slide1'   : { val : false, wait: 0     },
      'img1'     : { val : false, wait: 200   },
      'text1'    : { val : false, wait: 400   },
      'slide2'   : { val : false, wait: 800   },
      'img2'     : { val : false, wait: 600   },
      'text2'    : { val : false, wait: 600   },
      'slide3'   : { val : false, wait: 800   },
      'img3'     : { val : false, wait: 600   },
      'text3'    : { val : false, wait: 600   },
      'img4'     : { val : false, wait: 1000  },
      'sgobbo1'  : { val : false, wait: 600   },
      'sgobbo2'  : { val : false, wait: 600   },
      'sgobbo3'  : { val : false, wait: 600   },
      'sgobbo4'  : { val : false, wait: 600   },
    };
    let time = 0;
    // tslint:disable-next-line:forin
    for (const item in this.animations) {
      time += this.animations[item].wait;
      /*if (item.substr(0, 7) === 'dialogo') {
        setTimeout(_ => { this.avviaDialogo(item); } , time);
      }*/
      if (!this.animations[item].val) {
        setTimeout(_ => { this.animations[item].val = true; } , time);
      }
    }
    /*if (this.dataSer.userData) {
      if (this.dataSer.userData.medico) { // se è un medico
        this.router.navigate(['/DrManager']);
      } else if (this.dataSer.userData.medicRef && this.dataSer.userData.codFis) {
        // se è un user ed ha una sede associata e un codice fiscale
      //  this.router.navigate(['/home']);
      }
    } else {
      this.subUserData = this.dataSer.userData$.pipe(
        filter(val => val != null)
      )
      .subscribe(user => {
        this.currentUser = user;
        if (this.dataSer.userData.medico) { // se è un medico
          this.router.navigate(['/DrManager']);
        } else if (this.dataSer.userData.medicRef && this.dataSer.userData.codFis) {
          // se è un user ed ha una sede associata e un codice fiscale
            // verifico se la coppia (codFis, email) nel localStorage corrisponde a quella del currentuser
            // se si, confermo la verifica email e lo rimuovo dal localStorage
            this.myStorage = window.localStorage;
            this.emailDaVerificare= this.myStorage.getItem(this.currentUser.id);
            if( this.emailDaVerificare==this.currentUser.email){
              //aggiorno nel DB che l'email è verificata
              this.myStorage.removeItem(this.currentUser.id);
            }
            this.router.navigate(['/home']);
          }
      });
    }*/
  } // fine OnInit()
  /*avviaDialogo(item: string) {
    switch (item) {
      case 'dialogo1': {
        this.dialoga(this.dialogo1, 0, 1);
        break;
      }
      case 'dialogo2': {
        this.dialoga(this.dialogo2, 0, 2);
        break;
      }
      case 'dialogo3': {
        this.dialoga(this.dialogo3, 0, 3);
        break;
      }
    }
  }
  dialoga(text: string, i: number, diag: number) {
    if (text.length > 0) {
      switch (diag) {
        case 1: {this.dialogo1Show += text.charAt(i); break; }
        case 2: {this.dialogo2Show += text.charAt(i); break; }
        case 3: {this.dialogo3Show += text.charAt(i); break; }
      }
      text = text.substr(1, text.length);
    }
    setTimeout(_ => { this.dialoga(text, i++, diag); } , 30);
  }*/
}
