import { Injectable,  OnDestroy } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { auth } from 'firebase/app';
import { Observable, Subscription, of } from 'rxjs';
import { FormGroup  } from '@angular/forms';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument } from 'angularfire2/firestore';
import { map, filter, take } from 'rxjs/operators';
import { User } from './user';
import { Medico } from './medico';
import { UserDataService } from './user-data.service';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AutenticationService implements OnDestroy {
  userForm: FormGroup;
  user: firebase.User; // non contiene tutti i valori dell'user corrente, ma solo quelli relativi alle credenziali e UID
  UserCol: AngularFirestoreCollection<User | Medico>;
  UserDoc: AngularFirestoreDocument<User | Medico>;
  data: Observable<User | Medico>;
  subGoogle: Subscription;
  subCheckUser: Subscription;
  subPhone: Subscription;
  tab_attiva = false;
  windowRef: any;
  loginEsito = true;
  UserColWrite: AngularFirestoreCollection<User>; // provvisorio per il primo get
  MedicCol: AngularFirestoreCollection<Medico>;
  medicoLogin = false; // rinizializza medicologin ogni volta pekke senno attivano da login e fanno un macello
  subDataCheckUser: Subscription;
  emailGiaPresa = false;
  qualcosaDiStorto = false;
  codiceIncorretto = false;
  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private dataSer: UserDataService) {
      this.verificaTipoLogin();
      this.checkUserJoined();
  }
  verificaTipoLogin() {
    if (this.medicoLogin) { // check if I have to login a medic -> retrieving the right collection
      this.MedicCol = this.afs.collection('medici');
      this.UserCol = this.MedicCol;
    } else {
      this.UserColWrite = this.afs.collection('users');
      this.UserCol = this.UserColWrite;
    }
  }

  logOut() {
    this.afAuth.auth.signOut();
    this.user = null;
  }
  resetPassword(email: string) {
    return this.afAuth.auth.sendPasswordResetEmail(email)
      .then(() => console.log('sent Password Reset Email!'))
      .catch((error) => console.log(error));
  }
  sendLoginCode(num, appVerifier, windowRef) {
    this.windowRef = windowRef;
    this.afAuth.auth.signInWithPhoneNumber(num, appVerifier)
      .then(result => {
        console.log(result);
        this.windowRef.confirmationResult = result;
        this.subCheckUser.unsubscribe();
      })
      .catch( error => {
        this.qualcosaDiStorto = true;
        console.log(error);
      });
  }
  verifyLoginCode(verificationCode) {
    this.windowRef.confirmationResult
      .confirm(verificationCode)
      .then( result => {
        this.user = result.user;
        console.log('utente registrato con telefono');
        this.UserDoc = this.UserColWrite.doc(this.user.uid);
        this.data = this.UserDoc.snapshotChanges().pipe(
          map(action => {
            if (action.payload.exists === false) { // teoricamente impossibile
              console.log('login/signup con il telefono e non abbia un documento');
              return null;
            } else {
              const data = action.payload.data();
              const id = action.payload.id;
              return { id, ...data };
            }
          })
        );
        this.subPhone = this.data.pipe(take(1)).subscribe( user => {
          this.router.navigate(['/account']);
          if (!user) { // non ce user
            const data = {
              phoneNumber : this.user.phoneNumber,
              medico: false,
              nome: '',
              cognome: '',
              id: user.id
            };
            this.UserColWrite.doc(this.user.uid).set(data).then(() => this.updateDatiCurrentUser(data));
          } else {
            this.updateDatiCurrentUser(user);
          }
        });
      })
    .catch( error => {
      this.codiceIncorretto = true;
      console.log(error, 'Incorrect code entered?');
    });
  }
  emailLogin(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
    .then(credential => {
      this.subCheckUser.unsubscribe();
      // this.user.uid = credential.user.uid;
      console.log(credential.user.uid + '  > email login');
      this.UserDoc = this.UserCol.doc(credential.user.uid);
      this.data = this.UserDoc.snapshotChanges().pipe(
        map(action => {
          if (action.payload.exists === false) { // teoricamente impossibile
            return null;
          } else {
            this.user = credential.user;
            const data = action.payload.data();
            const id = action.payload.id;
            return { id, ...data };
          }
        })
      );
      this.data.pipe(filter(user => user !== null), take(1))
      .subscribe( user => {
        this.updateDatiCurrentUser(user);
      });
    })
    .catch(err => {
      console.log(err + '  > email login');
      this.loginEsito = false;
    });
  }
  anonymousLogin() {
    return this.afAuth.auth.signInAnonymously();
  }
  createUserProfile() {
    const data = {
      email : this.user.email,
      medico: false,
      nome: '',
      cognome: '',
      id: this.user.uid
    };
    this.UserColWrite.doc(this.user.uid).set(data).then(() => this.updateDatiCurrentUser(data));
  }
  checkUserJoined() {
    this.subCheckUser = this.afAuth.authState.pipe(filter(user => user !== null)) // ritorna firebase.user da authentication
    .subscribe(userData => {
      // console.log(userData.uid + 'provanull');
      if (userData) { // impedire che checkuserjoined() funzioni se mi loggo manualmente
        this.user = userData;
        console.log('utente joined' + this.user.uid);
        this.UserDoc = this.UserCol.doc(this.user.uid);
        this.data = this.UserDoc.snapshotChanges().pipe(
          map(action => {
            if (action.payload.exists === false) { // teoricamente impossibile
              if (!this.medicoLogin) { // se il documento non esiste vuol dire che l'utente che accede è un medico
                this.medicoLogin = true;
                this.verificaTipoLogin();
                this.subCheckUser.unsubscribe();
                this.checkUserJoined();
                console.log('cambio tipologin' + this.medicoLogin);
              }
              console.log('impossibile che uno joinato  non abbia un documento');
              return null;
            } else {
              const data = action.payload.data();
              const id = action.payload.id;
              return { id, ...data };
            }
          })
        );
        this.subDataCheckUser = this.data.pipe(filter(user => user !== null), take(1))
        .subscribe(user => {
          this.updateDatiCurrentUser(user);
          this.subCheckUser.unsubscribe();
        });
      }
    });
  }
  checkWhenUserJoined(): Observable<firebase.User> {
    return this.afAuth.authState;
  }
  emailSignUp(email, password) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then(credential => {
        this.subCheckUser.unsubscribe();
        console.log('utente creato in autentication con email');
        this.user = credential.user;
        this.router.navigate(['/account']);
        this.createUserProfile();
      }).catch(err => {
        this.emailGiaPresa = true;
        console.log(err + '  > email signup');
      });
  }

  linkGoogle() { // trasformare account anonimo in account google
    const provider = new auth.GoogleAuthProvider();
    auth().currentUser.linkWithPopup(provider);
    // this.createUserProfileGoogle();
  }
  googleLogin() {
    const provider = new auth.GoogleAuthProvider();
    return this.socialSignIn(provider);
  }
  socialSignIn(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
    .then(credential => {
      this.subCheckUser.unsubscribe();
      this.user = credential.user;
      console.log('creato utente in authenticaton');
      this.createUserProfileGoogle();
    })
    .catch(error => this.handleError(error));
    // return this.afAuth.auth.signInWithRedirect(provider); //alternativa
  }
  createUserProfileGoogle() {
    this.UserDoc = this.UserColWrite.doc(this.user.uid);
    this.data = this.UserDoc.snapshotChanges().pipe(
      map(action => {
        if (action.payload.exists === false) { // check if exist
          return null;
        } else {
          const data = action.payload.data();
          const id = action.payload.id;
          return { id, ...data };
        }
      })
    );
    this.subGoogle = this.data.pipe(take(1)).subscribe(val => {
      this.router.navigate(['/account']);
      if (!val) { // non esiste il documento
        // mostra campo fiscale e spiegazione
        const data = {       // qua devi fa il macello per il codice fiscale
          email : this.user.email,
          nome: '',
          cognome: '',
          medico: false,
          id: this.user.uid
        };
        this.UserColWrite.doc(this.user.uid).set(data).then(() => this.updateDatiCurrentUser(data));
      } else {      // c'è gia il documento
        this.updateDatiCurrentUser(val);
      }
    });
  }
  handleError(error) {
    this.qualcosaDiStorto = true;
    console.log(error);
  }
  updateDatiCurrentUser(user: any) {
    this.dataSer.updateDatiCurrentUser(user);
  }
  ngOnDestroy() {
    if (this.subCheckUser) { this.subCheckUser.unsubscribe(); }
  }
}
