import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartListAppuntamentiComponent } from './smart-list-appuntamenti.component';

describe('SmartListAppuntamentiComponent', () => {
  let component: SmartListAppuntamentiComponent;
  let fixture: ComponentFixture<SmartListAppuntamentiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartListAppuntamentiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartListAppuntamentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
