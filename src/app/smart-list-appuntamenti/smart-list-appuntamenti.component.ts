import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../user-data.service';
import { CalendarioService } from '../calendario.service';
import { Medico } from '../medico';
import { filter, take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-smart-list-appuntamenti',
  templateUrl: './smart-list-appuntamenti.component.html',
  styleUrls: ['./smart-list-appuntamenti.component.css']
})
export class SmartListAppuntamentiComponent implements OnInit {
  medico: Medico;
  modSala: string;
  subUserData: Subscription;
  orderList: any[] = [
    {value: 'inizio', viewValue: 'data'},
    {value: 'confermato', viewValue: 'conferma'},
  ];
  constructor(private dataSer: UserDataService, public calSer: CalendarioService) { }

  ngOnInit() {
    if (this.dataSer.userData) {
      this.medico = this.dataSer.userData;
      if (!this.medico.orarioRicette) { this.medico.orarioRicette = ''; }
      if (!this.medico.durataAppuntamenti) { this.medico.durataAppuntamenti = 20; }
      this.modSala = this.dataSer.modSala;
    } else {
      this.subUserData = this.dataSer.userData$.pipe(
        filter(val => val != null), take(1)
      )
      .subscribe(user => {
        this.medico = user;
        if (!this.medico.orarioRicette) { this.medico.orarioRicette = ''; }
        if (!this.medico.durataAppuntamenti) { this.medico.durataAppuntamenti = 20; }
        this.modSala = this.dataSer.modSala;
        // this.subUserData.unsubscribe();
      });
    }
  }
  riordinaEventi($event: string) {
    this.calSer.getEventiSede(this.dataSer.medico.id, false, $event, 'asc').pipe(take(1)).subscribe();
  }

}
