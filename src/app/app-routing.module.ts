import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { DrSettingsComponent } from './dr-settings/dr-settings.component';
import { DrQueueComponent } from './dr-queue/dr-queue.component';
import { HomeUtenteComponent } from './HomeUtente/HomeUtente.component';
import { VisitaComponent } from './visita/visita.component';
import { RicettaComponent } from './ricetta/ricetta.component';
import { AppuntamentoComponent } from './appuntamento/appuntamento.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { AppuntamentiManagerComponent } from './appuntamenti-manager/appuntamenti-manager.component';
import { WhitepaperComponent } from './whitepaper/whitepaper.component';
const routes: Routes = [
  { path: 'account/signup', component: SignUpComponent, data: {animation: 'sign'} },
  { path: 'account/login' , component: LoginComponent, data: {animation: 'login'} },
  { path: 'DrManager' , component: DrQueueComponent, data: {animation: 'queue'} },
  { path: 'DrSettings' , component: DrSettingsComponent, data: {animation: 'settings'} },
  { path: 'checkMe' , component: HomeComponent, data: {animation: 'home'} },             // e vedi se check user joined va cambiato
  { path: 'home' , component: HomeUtenteComponent, data: {animation: 'homeut'} },
  { path: 'home/visita' , component: VisitaComponent, data: {animation: 'visita'}  },
  { path: 'home/ricetta' , component: RicettaComponent, data: {animation: 'ricetta'}  },
  { path: 'home/appuntamento' , component: AppuntamentoComponent, data: {animation: 'appuntamento'}  },
  { path: 'account' , component: MyAccountComponent, data: {animation: 'account'}  },
  { path: 'AppuntamentiMng' , component: AppuntamentiManagerComponent, data: {animation: 'appuntamenti'}  },
  { path: 'whitePaper' , component: WhitepaperComponent, data: {animation: 'white'}  },
  { path: '' , redirectTo: '/checkMe', pathMatch: 'full' }

]; // doppio captcha BUG
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }


