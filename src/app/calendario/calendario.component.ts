import { Component, OnInit, ChangeDetectorRef, Inject, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { CalendarEvent, DayViewHourSegment } from 'calendar-utils';
import { CalendarEventTimesChangedEvent } from 'angular-calendar';
import { AngularFirestoreDocument } from 'angularfire2/firestore';
import { Medico } from '../medico';
import { take, mergeMap } from 'rxjs/operators';
import { UserDataService } from '../user-data.service';
import { CalendarioService } from '../calendario.service';
import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.css']
})
export class CalendarioComponent implements OnInit, OnDestroy {
  medDoc: AngularFirestoreDocument<Medico>;
  medObs: Observable<Medico>;
  subMed: Subscription;
  subUserData: Subscription;
  medico: Medico;
  // orari: Orari;
  // orariAppuntamenti: Orari;
  subOrari: Subscription;
  subOrariApp: Subscription;
  subEventiApp: Subscription;
  activeDayIsOpen: boolean;
  modalData: { event: CalendarEvent<any>; action: string; };
  modal: any;
  dragToCreateActive = false;
  // tslint:disable-next-line:max-line-length
  constructor(private dataSer: UserDataService, private CDR: ChangeDetectorRef, public calSer: CalendarioService, private bottomSheet: MatBottomSheet) { }
  ngOnInit() {
    if (this.dataSer.userData) {
      this.medico = this.dataSer.medico;
      this.calSer.getOrarioSede(this.medico.id).pipe(take(1)).subscribe();
      this.subEventiApp = this.calSer.getEventiSede(this.medico.id).subscribe();
    } else {
      this.subUserData = this.dataSer.userData$.pipe(
        mergeMap(val =>
          this.calSer.getEventiSede(this.dataSer.medico.id)
        ),
        mergeMap(val =>
          this.calSer.getOrarioSede(this.dataSer.medico.id).pipe(take(1))
        ),
      ).subscribe(val => {
        this.medico = this.dataSer.medico;
      });
    }
  }
  deleteEvento(id: string, numEvento: string) {
    this.calSer.deleteEvento(id, numEvento);
  }
  changeViewDate(action: string) {
    this.calSer.changeViewDate(action);
  }
  riempiIndisponibilita() { // riempie il giorno della settimana con evento indisponibilità
    this.calSer.riempiIndisponibilita();
  }
  deleteIndisponibilita() {
    this.calSer.deleteIndisponibilita();
  }
  calcolaInizioCal(): number {
    return this.calSer.calcolaInizioCal();
  }
  calcolaFineCal(): number {
    return this.calSer.calcolaFineCal();
  }
  /*dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }*/

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    console.log('timechange');
    this.handleEvent('Dropped or resized', event);
    this.calSer.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    let data;
    for (const item in this.calSer.events) { // ritrova l'evento con interfaccia adatta al DB
      if (this.calSer.events[item].start === event.start ) {
        data = {data: this.calSer.events[item], index: item};
      }
    }
    this.calSer.getAppuntamento(data.data.id).pipe(take(1)).subscribe(val => {
      data = {data: val, index: data.index};
      if (!event.cssClass) {this.openBottomSheet({data}); } // evito cssClass perchè sono gli eventi per no-disponibile
    });
  }

  /*addEvent(): void {
    const data_inizio = new Date();
    const data_fine = new Date(data_inizio.getTime() + this.medico.durataAppuntamenti * 60000);
    this.events.push({
      title: 'New event',
      start: data_inizio,
      end: data_fine,
      color: this.colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    });
    this.refresh.next();
  }*/
  startClickToCreate(segment: DayViewHourSegment, mouseDownEvent: MouseEvent, segmentElement: HTMLElement) {
    this.CDR.detectChanges();
    this.calSer.startClickToCreate(segment, mouseDownEvent, segmentElement);
  }
  openBottomSheet(data: any): void {
    this.bottomSheet.open(EventInfoModal, data);
  }
  ngOnDestroy(): void {
    if (this.subEventiApp) { this.subEventiApp.unsubscribe(); }
    if (this.subUserData) { this.subUserData.unsubscribe(); }
  }
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bottom-modal-event',
  templateUrl: 'bottom-modal-event.html',
})
// tslint:disable-next-line:component-class-suffix
export class EventInfoModal {
  // tslint:disable-next-line:max-line-length
  constructor(public calSer: CalendarioService, private bottomSheetRef: MatBottomSheetRef<EventInfoModal>, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
    console.log(data);
  }
  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
  deleteEvento(id: string, numEvento: string) {
    this.calSer.deleteEvento(id, numEvento);
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
  confermaEvento(id: string, numEvento: string) {
    this.calSer.confermaEvento(id, numEvento);
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
  updateNominativo(id: string, nome: string) {
    this.calSer.updateNominativo(id, nome);
  }
  updateInfo(id: string, info: string) {
    this.calSer.updateInfo(id, info);
  }
}
