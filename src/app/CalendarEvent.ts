import { EventColor, EventAction } from 'calendar-utils';

export interface CalendarEvent<MetaType = any> {
    start: Date;
    end?: Date;
    title: string;
    color: EventColor;
    actions?: EventAction[];
    allDay?: boolean;
    confermato?: boolean;
    cssClass?: string;
    resizable?: {
      beforeStart?: boolean;
      afterEnd?: boolean;
    };
    draggable?: boolean;
    meta?: MetaType;
}
