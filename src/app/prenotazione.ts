import { Timestamp } from '@google-cloud/firestore';
import { Reference } from 'angularfire2/firestore';
export interface Prenotazione {
    UserRef?: Reference<any>;
    tipo?: string;
    TimeIns?: Timestamp;
    numeroProgressivo?: Number; // da non mostrare al cliente, serve a noi nel db
    dataAppuntamento?: Timestamp;
    dataInizio?: Timestamp;
    dataFine?: Timestamp; // in base a quella impostata dal dottore o calcolata
    codiceAttesa?: number; // codice indentificazione nella coda, non corrisponde per forza all'ordine effettivo
    indicazioni?: string;
    nominativo?: string;
    codFis?: string;
}
