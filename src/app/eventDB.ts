import { DocumentReference } from 'angularfire2/firestore';
import { Timestamp } from '@google-cloud/firestore';

export interface EventDB {
    inizio: Timestamp;
    id: string; // id documento
    userRef: DocumentReference;
    confermato: string;
    messaggio: string;
    nominativo: string;
    fromMedico: boolean;
}
