import { Injectable } from '@angular/core';
// import { AutenticationService } from './autentication.service';
import { Medico } from './medico';
import { User } from './user';
import { Observable, Subject, Subscription } from 'rxjs';
import { Prenotazione } from './prenotazione';
import { DBmanagerService } from './dbmanager.service';
import { filter, map, take} from 'rxjs/operators';
import { EventDB } from './eventDB';
import { MatSnackBar } from '@angular/material';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { AngularFirestoreDocument, DocumentReference } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  userData: any;
  userData$: Subject<any>;
  eventi$: Observable<EventDB[]>;
  eventi: EventDB [];
  medico: Medico;
  user: User;
  sedeOpen: boolean;
  currentUser: User;
  medicDoc: AngularFirestoreDocument<any>;
  userObs: Observable<User>;
  iscritto: Subscription;
  miePrenotazioni: number;
  idMiaPrenotazione: string;
  modSala: string;
  codiceAttesaMiaPrenotazione: number;
  myStorage: any;
  filePath: string;
  UploadTask: AngularFireUploadTask;
  medicRef: DocumentReference;
  sedeRef: string;
  constructor(private DB: DBmanagerService, public snackBar: MatSnackBar, public storage: AngularFireStorage) {
    this.userData$ = new Subject();
    this.myStorage = window.localStorage;
  }
  updateVisitCounter() {
    const doc = this.DB.getDocument('AppInfo/gina');
    const obs = doc.valueChanges().pipe(take(1));
    obs.subscribe(val => {
      if (val.hit_counter) {
        this.DB.updateDocument('AppInfo/gina', {hit_counter: val.hit_counter + 1});
      } else {
        this.DB.updateDocument('AppInfo/gina', {hit_counter: 1});
      }
    });
  }
  updateDatiCurrentUser(user: any) {
    this.userData = user;
    if (this.userData.medico)  {
      this.medico = this.userData; // se ha loggato il medico, lo metto dentro this.medico
      this.user = this.userData;
      this.userData$.next(user);
    } else {
      this.user = this.userData;
      if (this.user.medicRef) {
        this.medicDoc = this.DB.getDocument(this.user.medicRef.path);
        this.medicDoc.snapshotChanges().pipe(
          take(1),
          map(action => { // mi prendo l'id e lo metto nell'ogetto
            if (action.payload.exists === false) { // teoricamente impossibile
              return null;
            } else {
              const data = action.payload.data();
              const id = action.payload.id;
              return { id, ...data };
            }
          })
          ).subscribe(val => {
          this.medico = val;
          this.userData$.next(user);
        });
      } else {
        this.userData$.next(user);
      }
    }
  }
  cambiaSede(sede: string) {
    this.medico.sedeRef = sede;
    this.user.sedeRef = sede;
    this.userData.sedeRef = sede;
    this.DB.updateDocument(`medici/${this.medico.id}`, {sedeRef: sede});
  }
  aggiungiNuovaSede(name): any {
    const objOrari = {
      orariAppuntamenti: {
        'lun' : { open : '', close: ''},
        'mar' : { open : '', close: ''},
        'mer' : { open : '', close: ''},
        'gio' : { open : '', close: ''},
        'ven' : { open : '', close: ''},
        'sab' : { open : '', close: ''},
        'dom' : { open : '', close: ''},
        'lun2': { open : '', close: ''},
        'mar2': { open : '', close: ''},
        'mer2': { open : '', close: ''},
        'gio2': { open : '', close: ''},
        'ven2': { open : '', close: ''},
        'sab2': { open : '', close: ''},
        'dom2': { open : '', close: ''}
      },
      orari: {
        'lun' : { open : '', close: ''},
        'mar' : { open : '', close: ''},
        'mer' : { open : '', close: ''},
        'gio' : { open : '', close: ''},
        'ven' : { open : '', close: ''},
        'sab' : { open : '', close: ''},
        'dom' : { open : '', close: ''},
        'lun2': { open : '', close: ''},
        'mar2': { open : '', close: ''},
        'mer2': { open : '', close: ''},
        'gio2': { open : '', close: ''},
        'ven2': { open : '', close: ''},
        'sab2': { open : '', close: ''},
        'dom2': { open : '', close: ''}
      }
    };
    this.medico.sedi.push(name);
    this.DB.updateDocument(`medici/${this.medico.id}`, {sedi: this.medico.sedi});
    this.DB.updateDocument(`medici/${this.medico.id}`, {sedeRef: name}); // aggiungi a sede attuale
    this.medico.sedeRef = name;
    this.DB.getCollection(`medici/${this.medico.id}/sede`).doc(name).collection<any>('coda').doc('try').set({a : ''})
    .then(function() {
        console.log('Pages Created successfully!');
    })
    .catch(function(error) {
        console.error('Error writing document: ', error);
    });
    this.DB.getCollection(`medici/${this.medico.id}/sede`).doc(name).collection<any>('orari').doc('orari').set(objOrari)
    .then(function() {
        console.log('Pages Created successfully!');
    })
    .catch(function(error) {
        console.error('Error writing document: ', error);
    });
    this.DB.getCollection(`medici/${this.medico.id}/sede`).doc(name).collection<any>('appuntamenti').doc('try').set({a: ''})
    .then(function() {
        console.log('Pages Created successfully!');
    })
    .catch(function(error) {
        console.error('Error writing document: ', error);
    });
  }
  linkMedicoAndSede(idUt: string, medicRef: DocumentReference, sedeRef: string) {
    this.DB.updateDocument(`users/${idUt}`, {medicRef: medicRef, sedeRef: sedeRef});
    this.user.medicRef = medicRef;
    this.user.sedeRef = sedeRef;
    this.updateDatiCurrentUser(this.user);
  }
  checkEsistenzaSede($event): any {
    let returnval = null;
    // console.log($event.target.value);
    const col = this.DB.getQueryedCollection('medici', 'sedi', 'array-contains', $event.target.value);
    const obs = col.valueChanges().pipe(
      map(val => {
        if (val.length === 0) {
          return false;
        } else {
          this.sedeRef = $event.target.value;
          this.medicRef = val[0].medicRef;
          return true;
        }
        }),
      take(1));
    returnval = obs;
    return returnval;
  }
  upload(file: File): any {
    this.filePath = this.medico.id + '/' + this.user.id;
    return this.storage.upload(this.filePath, file);
  }
  verificaModalitaSala() {
    this.modSala = this.myStorage.getItem('modSala');
    if (this.modSala == null) {
      this.modSala = 'false';
    }
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
  getNumProgressivo(): Observable<any> {
    const col = this.DB.getOrderedCollection(this.user.medicRef.path + '/sede/' + this.user.sedeRef + '/coda', 'numeroProgressivo');
    const codaObs = col.snapshotChanges().pipe(
      map(arr => {
        return arr.map(snap => {
          const data = snap.payload.doc.data();
          const id = snap.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return codaObs;
  }
  getCodiceAttesa(): Observable<any> {
    const col = this.DB.getOrderedCollection(this.user.medicRef.path + '/sede/' + this.user.sedeRef + '/coda', 'codiceAttesa');
    const codaObs = col.snapshotChanges().pipe(
      map(arr => {
        return arr.map(snap => {
          const data = snap.payload.doc.data();
          const id = snap.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return codaObs;
  }
  checkMiaPrenotazione(): Observable<any> {
    // tslint:disable-next-line:max-line-length
    const col = this.DB.getQueryedCollection(this.user.medicRef.path + '/sede/' + this.user.sedeRef + '/coda', 'UserRef', '==', this.createUserRef());
    const codaObs = col.snapshotChanges().pipe(
      filter(val => val !== null),
      map(arr => {
        this.miePrenotazioni = arr.length;
        return arr.map(snap => {
          const data = snap.payload.doc.data();
          const id = snap.payload.doc.id;
          this.idMiaPrenotazione = id;
          this.codiceAttesaMiaPrenotazione = data.codiceAttesa;
          return { id, ...data };
        });
      })
    );
    return codaObs;
  }
  aggiungiAllaCoda(data: Prenotazione) { // aggiunge le visite e ricette alla coda
    data.TimeIns = this.DB.getTimestamp(); // set timestamp manualmente dopo
    this.DB.addToCollection(this.user.medicRef.path + '/sede/' + this.user.sedeRef + '/coda', data);
  }
  createUserRef(): DocumentReference {
    const cud = this.DB.getDocument('users/' + this.user.id);
    return cud.ref;
  }
  setNomeUser(nome: string) {
    this.user.nome = nome;
    this.DB.updateDocument(`users/${this.user.id}`, {nome: nome});
  }
  setCognomeUser(cognome: string) {
    console.log(cognome);
    this.user.cognome = cognome;
    this.DB.updateDocument(`users/${this.user.id}`, {cognome: cognome});
  }
  setcodFisUser(codFis) {
    this.user.cognome = codFis;
    this.DB.updateDocument(`users/${this.user.id}`, {codFis: codFis});
  }
}
