import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../user';
import { Observable, Subscription, of } from 'rxjs';
  import { Medico } from '../medico';
  import { DBmanagerService } from '../dbmanager.service';
  import { map, filter, mergeMap, take } from 'rxjs/operators';
  import { UserDataService } from '../user-data.service';
import { CalendarioService } from '../calendario.service';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-HomeUtente',
  templateUrl: './HomeUtente.component.html',
  styleUrls: ['./HomeUtente.component.css']
})
export class HomeUtenteComponent implements OnInit, OnDestroy {
  medico: Medico;
  currentUser: User;
  UserObs: Observable<User>;
  subUserData: Subscription;
  numMiePrenotazioni: number;
  actionCodeSettings: any;
  // myStorage: any;
  lunghezzaCoda: number;
  numRicette = 0;
  numVisite = 0;
  elenco$: Observable<any[]>;
  elenco: any[];
  genteInCoda: number;
  minutiStimati: number;
  idMiaPrenotazione: string;
  codiceAttesaMiaPrenotazione: number;
  codiceAttesaAttutale: number;
  subCoda: Subscription;
  subMiaPren: Subscription;
  orariSubcribe: Subscription;
  constructor(public dataSer: UserDataService, private DB: DBmanagerService, private calSer: CalendarioService) { }
  ngOnInit() { // se non c'è medic ref all'utente, la variabile dataSer.medico è vuota
    // this.myStorage = window.localStorage;
    if (this.dataSer.userData) {
      if (this.dataSer.userData.medicRef) {
        this.currentUser = this.dataSer.user;
        this.medico = this.dataSer.medico;
        this.orariSubcribe = this.calSer.getOrarioSede(this.dataSer.medico.id).subscribe(_ => {
          this.dataSer.sedeOpen = this.calSer.checkStudioAperto();
        });
        if (this.dataSer.modSala === 'false') {
          this.subMiaPren = this.checkMiaPrenotazione().subscribe(val => {
            this.numMiePrenotazioni = val.length;
            this.idMiaPrenotazione = this.dataSer.idMiaPrenotazione;
            this.codiceAttesaMiaPrenotazione = this.dataSer.codiceAttesaMiaPrenotazione;
          });
        }
        this.subCoda = this.getCoda(this.medico.id).subscribe(_ => {
          if (this.dataSer.modSala === 'false') {
            this.formatDatiCoda();
          }
        });
      }
    } else {
      this.subUserData = this.dataSer.userData$.pipe(
        filter(val => val !== undefined),
        mergeMap(_ => {let a; if (this.dataSer.medico) {a = this.getCoda(this.dataSer.medico.id); } else {a = of(''); } return a; }),
        mergeMap(_ => {let a; if (this.dataSer.medico) {a = this.checkMiaPrenotazione();          } else {a = of(''); } return a; }),
        // tslint:disable-next-line:max-line-length
        mergeMap(_ => {let a; if (this.dataSer.medico) {a = this.calSer.getOrarioSede(this.dataSer.medico.id); } else {a = of(''); } return a; })
      )
      .subscribe(user => {
        if (this.dataSer.userData.medicRef) {
          this.currentUser = this.dataSer.user;
          this.medico = this.dataSer.medico;
          if (this.dataSer.modSala === 'false') {
            this.numMiePrenotazioni = this.dataSer.miePrenotazioni;
            this.idMiaPrenotazione = this.dataSer.idMiaPrenotazione;
            this.codiceAttesaMiaPrenotazione = this.dataSer.codiceAttesaMiaPrenotazione;
            /*this.actionCodeSettings = {
              url: 'https://tostissimo-46435.firebaseapp.com/email=' + firebase.auth().currentUser.email,
              handleCodeInApp: false,
            }*/
            this.formatDatiCoda();
          }
          this.dataSer.sedeOpen = this.calSer.checkStudioAperto();
        }
      });
    }
  }
  formatDatiCoda() {
    this.numRicette = 0;
    this.numVisite = 0;
    for (const item in this.elenco) {
      if (this.elenco[item].payload.doc.id !== this.idMiaPrenotazione) { // mia prenotazione è la prenotazione del currentUser
        const dataPren = this.elenco[item].payload.doc.data();    // così posso vedere quanta gente c'è davanti a lui
        if (dataPren.tipo === 'ricetta') {
          this.numRicette ++;
        } else if (dataPren.tipo === 'visita') {
          this.numVisite ++;
        }
      } else if (this.elenco[item].payload.doc.id === this.idMiaPrenotazione) { // scorri finchè non arrivi alla mia prenotazione
        break;
      }
    }
  }
  getCoda(idMed: string): Observable<any[]> {
    // tslint:disable-next-line:max-line-length
    const elencoCol = this.DB.getOrderedCollection('medici/' +  idMed + '/sede/' + this.dataSer.user.sedeRef + '/coda', 'numeroProgressivo', 'asc');
    return this.elenco$ = elencoCol.snapshotChanges().pipe(
      map(arr => {
        this.elenco = arr;
        this.lunghezzaCoda = arr.length;
        // this.arrayPren = {}; TODO
        this.numRicette = 0;
        this.numVisite = 0;
        if (this.lunghezzaCoda > 0) {
          this.codiceAttesaAttutale = arr[0].payload.doc.data().codiceAttesa;
        }
        return arr.map(snap => {
          const data = snap.payload.doc.data();
          const id = snap.payload.doc.id;
          if (data.tipo === 'ricetta') {this.numRicette ++; }
          if (data.tipo === 'visita') {this.numVisite ++; }
          return { id, ...data };
        });
      })
    );
  }
  checkMiaPrenotazione(): Observable<any> {
    if (this.dataSer.modSala === 'false') {
      return this.dataSer.checkMiaPrenotazione();
    } else {
      return of('') ;
    }
  }
  ngOnDestroy(): void {
    if (this.subUserData) { this.subUserData.unsubscribe(); }
    if (this.subMiaPren) { this.subMiaPren.unsubscribe(); }
    if (this.subCoda) { this.subCoda.unsubscribe(); }
    if (this.orariSubcribe) { this.orariSubcribe.unsubscribe(); }
  }
  /*verificaEmail(){
    var user = firebase.auth().currentUser;
    console.log({user});
    user.sendEmailVerification(this.actionCodeSettings).then(
      (success)=>{
        console.log("email inviata");
        this.myStorage.setItem(this.currentUser.id,this.currentUser.email);
      }
    ).catch(
      (err)=>{console.log("errore")}
    )
  }*/
}

// allenboom80@gmail.com
// madonnacane88
// ovunaque non c'è updatedatacurrentuser -> auth
// else ->databse
