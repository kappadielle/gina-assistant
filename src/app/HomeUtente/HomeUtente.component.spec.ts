import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeUtenteComponent } from './HomeUtente.component';

describe('HomeUtenteComponent', () => {
  let component: HomeUtenteComponent;
  let fixture: ComponentFixture<HomeUtenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeUtenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeUtenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
