import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatToolbarModule } from '@angular/material/toolbar';
import { DrSettingsComponent } from './dr-settings/dr-settings.component';
import { DrQueueComponent } from './dr-queue/dr-queue.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { HomeComponent } from './home/home.component';
import { HomeUtenteComponent } from './HomeUtente/HomeUtente.component';
import { VisitaComponent } from './visita/visita.component';
import { RicettaComponent } from './ricetta/ricetta.component';
import { AppuntamentoComponent } from './appuntamento/appuntamento.component';
import { MatTabsModule } from '@angular/material/tabs';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { SortableDirective } from './draggable/sortable.directive';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatChipsModule } from '@angular/material/chips';
import { CalendarioComponent, EventInfoModal } from './calendario/calendario.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatNativeDateModule } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { PickerAppuntamentiComponent } from './picker-appuntamenti/picker-appuntamenti.component';
import { OrariComponent } from './orari/orari.component';
import { MyAccountComponent, EventInfoModalsede } from './my-account/my-account.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { WebcamModule } from 'ngx-webcam';
import { AppuntamentiManagerComponent } from './appuntamenti-manager/appuntamenti-manager.component';
import { SmartListAppuntamentiComponent } from './smart-list-appuntamenti/smart-list-appuntamenti.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { LayoutModule } from '@angular/cdk/layout';
import { CodiceValidatorDirective } from './my-account/codiceValidator';
import { MatMenuModule } from '@angular/material/menu';
import { AutofocusModule } from 'angular-autofocus-fix';
import { WhitepaperComponent } from './whitepaper/whitepaper.component';
export const firebaseConfig = environment.firebaseConfig;

@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    LoginComponent,
    DrSettingsComponent,
    DrQueueComponent,
    HomeComponent,
    HomeUtenteComponent,
    VisitaComponent,
    RicettaComponent,
    AppuntamentoComponent,
    SortableDirective,
    EventInfoModal,
    CalendarioComponent,
    PickerAppuntamentiComponent,
    OrariComponent,
    MyAccountComponent,
    EventInfoModalsede,
    AppuntamentiManagerComponent,
    SmartListAppuntamentiComponent,
    CodiceValidatorDirective,
    WhitepaperComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    MatSlideToggleModule,  // sempre qua sotto
    MatChipsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatTabsModule,
    MatCardModule,
    MatButtonModule,
    MatBottomSheetModule,
    MatFormFieldModule,
    MatIconModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    // MatNativeDateModule,
    MatExpansionModule,
    MatMomentDateModule,
    MatSelectModule,
    MatDatepickerModule,
    LayoutModule,
    AutofocusModule,
    AngularFireStorageModule,
    // FlexLayoutModule,
    AppRoutingModule,
    WebcamModule,
    MatMenuModule,
    // WebStorageModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
  ],
  entryComponents: [EventInfoModal, EventInfoModalsede],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
