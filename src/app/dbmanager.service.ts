import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Timestamp, WhereFilterOp, OrderByDirection } from '@google-cloud/firestore';
@Injectable({
  providedIn: 'root'
})
export class DBmanagerService {

  constructor(private afs: AngularFirestore) { }
  getCollection(collection: string): AngularFirestoreCollection<any> {
    return this.afs.collection(collection);
  }
  getTimestamp(): Timestamp {
    return firebase.firestore.FieldValue.serverTimestamp() as Timestamp;
    // firebase.ServerValue()
  }
  // devi modificare la funzione in qualche modo
  getOrderedCollection(collection: string, fieldPath: string, mod: OrderByDirection = 'desc'): AngularFirestoreCollection<any> {
    return this.afs.collection(collection, ref => ref.orderBy(fieldPath, mod )); // asc default
  }
  getQueryedCollection(collection: string, field: string, operation: WhereFilterOp, val2: any): AngularFirestoreCollection<any> {
    return this.afs.collection(collection, ref => ref.where(field, operation, val2)); // query
  }
  getDocument(path: string): AngularFirestoreDocument<any> {
    return this.afs.doc(path);
  }
  addToCollection(collection: string, data: any) {
    const Col = this.getCollection(collection);
    Col.add(data)
      .then(() => console.log('aggiunto con successo'))
      .catch(err => console.log(`impossibile aggiungere documento alla collezione ${collection}, error: ` + err));
  }
  addToCollectionWithName(collection: string, data: any, name: string) {
    const Col = this.getCollection(collection);
    Col.doc(name).set(data)
      .then(() => console.log('aggiunto con successo'))
      .catch(err => console.log(`impossibile aggiungere documento alla collezione ${collection}, error: ` + err));
  }
  delDocument(path: string) {
    this.getDocument(path).delete();
  }
  updateDocument(path: string, data: any) { // aggiunge campi e aggiorna quelli gia presenti
    this.getDocument(path).update(data)
    .then(() => {console.log(`aggiornato con successo`); })
    .catch(err => console.log(`errore nell'aggiornamento, errore: ${err}`));
  }
}
// order by valore max, limit 1 cosi prendo il valore max
