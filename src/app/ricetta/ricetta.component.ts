import { Component, OnInit, OnDestroy, DoCheck } from '@angular/core';
import { User } from '../user';
import { Observable, Subscription, Subject } from 'rxjs';
import {
  AngularFirestoreCollection,
  DocumentReference} from 'angularfire2/firestore';
import { filter, take } from 'rxjs/operators';
import { Prenotazione } from '../prenotazione';
import { Router } from '@angular/router';
import { Medico } from '../medico';
import { UserDataService } from '../user-data.service';
import {WebcamImage, WebcamInitError} from 'ngx-webcam';
import { CalendarioService } from '../calendario.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
@Component({
  selector: 'app-ricetta',
  templateUrl: './ricetta.component.html',
  styleUrls: ['./ricetta.component.css']
})
export class RicettaComponent implements OnInit, OnDestroy, DoCheck {
  subCodAttesa: Subscription;
  subNumProgr: Subscription;
  subResponsive: Subscription;
  imgShowW: string = '0';
  imgShowH: string = '0';
  imgShowMT: string = '0';
  constructor(public dataSer: UserDataService, private router: Router, private calSer: CalendarioService, breakpointObserver: BreakpointObserver) {
    this.subResponsive = breakpointObserver.observe([
      Breakpoints.XSmall,
    ]).subscribe(result => {
      if (result.matches) {
        if (result.breakpoints) {
          if (result.breakpoints['(min-width: 600px) and (max-width: 839.99px) and (orientation: portrait)']) {
            console.log(result.breakpoints['(min-width: 600px) and (max-width: 839.99px) and (orientation: portrait)']);
          } else if (result.breakpoints['(max-width: 599.99px)']) {
            console.log(result.breakpoints['(max-width: 599.99px)']);
          }
        }
        this.videoOptions = {
          width: {exact: 123},
          height: {exact: 200},
        };
      } else {
        this.videoOptions = {
          width: {ideal: 700},
          height: {ideal: 500}
        };
      }
    });
  }
  maxNumProgressivo: number; // numero massimo visita di oggi
  iconCodFis: string;
  canUpload: boolean;
  listaPrenotazioni: Prenotazione[];
  numeroMassimo: number;
  CodaCol: AngularFirestoreCollection<Prenotazione>;
  progressivoGiusto: number;
  user: User;
  nomeMed = '';
  listaPrenotazioniCodAttesa: Prenotazione[];
  codiceAttesaMassimo: number;
  medico: Medico;
  nominativoArrivaNullo = false;
  ricettePerDomani = false;
  apriWebCam = false;
  public webcamImage: WebcamImage = null;
  private trigger: Subject<void> = new Subject<void>();
  public videoOptions: MediaTrackConstraints;
  selectedFiles: any;
  fileUploaded = false;
  arrivaCodFisNullo: boolean;
  percentuale: any;
  ngOnInit() {
    if (this.dataSer.userData) {
      this.user = this.dataSer.userData;
      if (this.dataSer.modSala === 'true') {
        this.user.codFis = '';
      }
      if (!this.user.codFis || this.user.codFis == '') {
        this.arrivaCodFisNullo = true;
      } else {
        this.arrivaCodFisNullo = false;
      }
      this.medico = this.dataSer.medico;
      this.calSer.getOrarioSede(this.dataSer.medico.id).pipe(take(1)).subscribe(_ => {
        this.dataSer.sedeOpen = this.calSer.checkStudioAperto();
      });
      this.checkDomaniRicette();
      this.getNumProgressivo();
      this.getCodiceAttesa();
      if (!this.user.nome) {this.user.nome = ''; }
      if (!this.user.cognome) {this.user.cognome = ''; }
      if (!this.user.cognome || !this.user.nome) {this.nominativoArrivaNullo = true; }
    } else {
      this.dataSer.userData$.pipe(
        filter(val => val != null), take(1)
      ).subscribe(user => {
        this.user = user;
        if (this.dataSer.modSala === 'true') {
          this.user.codFis = '';
        }
        if (!this.user.codFis || this.user.codFis == '') {
          this.arrivaCodFisNullo = true;
        } else {
          this.arrivaCodFisNullo = false;
        }
        this.medico = this.dataSer.medico;
        this.calSer.getOrarioSede(this.dataSer.medico.id).pipe(take(1)).subscribe(_ => {
          this.dataSer.sedeOpen = this.calSer.checkStudioAperto();
        });
        this.checkDomaniRicette();
        this.getNumProgressivo();
        this.getCodiceAttesa();
        if (!this.user.nome) {this.user.nome = ''; }
        if (!this.user.cognome) {this.user.cognome = ''; }
        if (!this.user.cognome || !this.user.nome) {this.nominativoArrivaNullo = true; }
      });
    }
  }
  apriWebcam() {
    this.apriWebCam = !this.apriWebCam;
  }
  checkDomaniRicette() {
    if (this.medico.ricetteStatiche) {
      const adesso = new Date();
      const adessoMin = adesso.getMinutes();
      const adessoHour = adesso.getHours();
      if (adessoHour > Number(this.medico.orarioRicette.substr(0, 2))) {
        this.ricettePerDomani = true;
      } else if (adessoHour == Number(this.medico.orarioRicette.substr(0, 2))) {
        if (adessoMin > Number(this.medico.orarioRicette.substr(3, 2))) {
          this.ricettePerDomani = true;
        }
      }
    }
  }
  updateCodFis($event) {
    const esit = /[A-Za-z]{6}[0-9]{2}[A-Za-z]{1}[0-9]{2}[A-Za-z]{1}[0-9]{3}[A-Za-z]{1}/.test($event.target.value);
    if (esit) {
      this.user.codFis = $event.target.value;
      this.dataSer.user.codFis = $event.target.value;
      this.iconCodFis = 'done';
      if (this.dataSer.modSala === 'false') {
        this.dataSer.setcodFisUser($event.target.value);
      }
    } else {
      this.iconCodFis = 'close';
    }
  }
  upload(file: File): any {
    const task = this.dataSer.upload(file);
    this.percentuale = task.percentageChanges();
    return task;
  }
  public handleImage(webcamImage: WebcamImage): void {
    this.webcamImage = webcamImage;
  }
  dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type: mime});
  }
  ngDoCheck() {
    const element = document.getElementById("imgShow");
    if (element) {
      const computedStyle = window.getComputedStyle(element, null);
      this.imgShowW = computedStyle.width;
      this.imgShowH = computedStyle.height;
      this.imgShowMT = computedStyle.marginTop;
    }
  }
  public triggerSnapshot(): void {
    this.trigger.next();
  }
  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }
  annullaImageCapture() {
    this.webcamImage = null;
    this.apriWebcam();
  }
  public handleInitError(error: WebcamInitError): void {
    console.log(error);
  }
  getNumProgressivo() {
    this.subNumProgr = this.dataSer.getNumProgressivo().pipe(
        filter(val => val != null)
      ).subscribe(mettiqua => {
        if (mettiqua.length) {
          this.listaPrenotazioni = mettiqua;
          let ultimaRicetta =  null;
          for (const item in this.listaPrenotazioni) {
            if (this.listaPrenotazioni[item].tipo === 'ricetta') {
              ultimaRicetta = this.listaPrenotazioni[item].numeroProgressivo;
              break;
            }
          }
          if (!ultimaRicetta) {
            this.numeroMassimo = Number(this.listaPrenotazioni[this.listaPrenotazioni.length - 1].numeroProgressivo);
          } else {
            this.numeroMassimo = ultimaRicetta;
          }
        } else {
          this.numeroMassimo = 0;
        }
    });
  }
  getCodiceAttesa() {
    this.subCodAttesa = this.dataSer.getCodiceAttesa().pipe(
        filter(val => val != null)
      ).subscribe(mettiqua => {
        if (mettiqua.length) {
          this.listaPrenotazioniCodAttesa = mettiqua;
          // tslint:disable-next-line:max-line-length
          this.codiceAttesaMassimo = Number(this.listaPrenotazioniCodAttesa[0].codiceAttesa + 1); // query dove prende il val max di codiceAttesa
        } else {
          this.codiceAttesaMassimo = 1;
        }
    });
  }
  getNumProgressivoGiusto(ricettaStatica: boolean): any {
    if (ricettaStatica) {
      return null;
    } else {
      return this.numeroMassimo + 0.01;
    }
  }
  aggiungiRicetta(nomemedicinale: string) {
    const data = {
      UserRef : this.createUserRef(),
      tipo : 'ricetta',
      TimeIns : null,
      nominativo: this.user.nome + ' ' + this.user.cognome,
      numeroProgressivo: this.getNumProgressivoGiusto(this.dataSer.medico.ricetteStatiche),
      indicazioni: nomemedicinale,
      codFis: this.user.codFis,
      codiceAttesa: this.codiceAttesaMassimo
    };
    // this.subscritto.unsubscribe();
    if (this.user.nome.length > 2 && this.user.cognome.length > 3 && this.nominativoArrivaNullo) {
      this.setNomeUser();
      this.setCognomeUser();
    }
    this.router.navigate(['/home']);
    this.dataSer.openSnackBar('Hai richiesto una ricetta', '');
    if (this.dataSer.modSala === 'true') {
      this.user.codFis = '';
    }
    this.dataSer.aggiungiAllaCoda(data);
    if (this.selectedFiles) {
      this.uploadSingle(); // upload immagine caricata manualmente
    } else if (this.webcamImage) {
      this.upload(this.dataURLtoFile(this.webcamImage.imageAsDataUrl, 'img.jpg'));
    }
  }
  createUserRef(): DocumentReference {
    return this.dataSer.createUserRef();
  }
  setNomeUser() {
    this.dataSer.setNomeUser(this.user.nome);
    this.user.nome = this.user.nome;
  }
  setCognomeUser() {
    this.dataSer.setCognomeUser(this.user.cognome);
    this.user.cognome = this.user.cognome;
  }
  detectFiles($event) {
    this.selectedFiles = $event.target.files;
    if (this.selectedFiles) {
      this.canUpload = true;
    }
  }
  uploadSingle() {
    const file: File = this.selectedFiles.item(0);
    const task = this.upload(file);
    this.fileUploaded = true;
    this.canUpload = false;
    return task;
  }
  ngOnDestroy(): void {
    if (this.subCodAttesa) { this.subCodAttesa.unsubscribe(); }
    if (this.subNumProgr) { this.subNumProgr.unsubscribe(); }
    if (this.subResponsive) { this.subResponsive.unsubscribe(); }
  }
  
}
