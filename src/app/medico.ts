import { DocumentReference } from 'angularfire2/firestore';

export interface Medico {
    email?: string;
    phoneNumber?: any;
    photoUrl?: string;
    codFis?: string; // uso del campo codice fiscale per codice medico così il login dell'user funziona pure per medico
    id?: string;  // non serve per inserimento ma per controllo documento e comodità
    ricetteStatiche?: boolean;
    codaSmart?: boolean;
    durataAppuntamenti?: number;
    medico?: boolean;
    orarioRicette?: string;
    nome?: string;
    cognome?: string;
    medicRef?: DocumentReference;
    sedeRef?: string;
    sedi: string[];
    accontCrowd?: boolean;
}
