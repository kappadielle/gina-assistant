import { TestBed, inject } from '@angular/core/testing';

import { DBmanagerService } from './dbmanager.service';

describe('DBmanagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DBmanagerService]
    });
  });

  it('should be created', inject([DBmanagerService], (service: DBmanagerService) => {
    expect(service).toBeTruthy();
  }));
});
