import { ValidatorFn, AbstractControl } from '@angular/forms';

export function EmailPhone_Validator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
        const emailpattern = /[a-zA-Z]+/;
        const phonepattern = /[0-9]+/;
        let emailphone = '';
        if (emailpattern.test(control.value)) {
            emailphone = 'email';
        } else if (phonepattern.test(control.value)) {
            emailphone = 'phone';
        }
        return emailphone ? {'custom': {value: control.value}} : null;
    };
}
//gesu è una bestia
