import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { AutenticationService } from '../autentication.service';
import { User } from '../user';
import { WindowService } from '../window.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  user: User;
  captchaShow = false;
  UserCol: AngularFirestoreCollection;
  labelTitle = 'Email o Telefono';
  windowRef: any;
  icon: string;
  verificationCode: string;
  hide = false;
  resetPassword = false;
  qualcosaDiStorto = false;
  constructor(public authSer: AutenticationService, private fb: FormBuilder, private win: WindowService) { }
  loginForm = this.fb.group({
    emailPhone:   ['', {
      validators: Validators.required,
      updateOn: 'blur'
    }],
    password:     ['', [Validators.required, Validators.minLength(6), Validators.maxLength(30),
                        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$')]]
  });

  ngOnInit() {
    this.authSer.medicoLogin = false;
    this.windowRef = this.win.windowRef;
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    this.windowRef.recaptchaVerifier.render();
    this.authSer.verificaTipoLogin();
  }
  phoneSendLoginCode() {
    const num = this.loginForm.value['emailPhone'];
    const appVerifier = this.windowRef.recaptchaVerifier;
    // num = '+393468302708';
    this.authSer.sendLoginCode(num, appVerifier, this.windowRef);
  }
  verifyLoginCode() {
    this.authSer.verifyLoginCode(this.verificationCode);
  }
  cambiaTipoAccesso($event) {
    console.log($event.checked);
    this.authSer.medicoLogin = $event.checked;
    this.authSer.verificaTipoLogin();
  }
  isValidMailFormat(email: string) {
    let returnval = false;
    if (email) {
      const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      if ((email.length === 0) && (!EMAIL_REGEXP.test(email))) {
        returnval = false;
      } else {
        returnval = true;
      }
    }
    return returnval;
  }
  sendResetEmail() {
    this.authSer.resetPassword(this.emailPhone.value)
      .then(() => this.resetPassword = true)
      .catch(_error => {
        this.qualcosaDiStorto = true;
      });
  }
  login() {
    const emailPhone = this.loginForm.value['emailPhone'];
    const password = this.loginForm.value['password'];
    if (this.icon === 'email' ) {
      this.authSer.emailLogin(emailPhone, password);
    } else if (this.icon === 'phone') {
      this.verifyLoginCode();
    }
  }
  googleLogin() {
    this.authSer.googleLogin();
  }

  updateValidators() {
    const val = this.loginForm.get('emailPhone');
    let valstring = val.value;
    const emailpattern = /[a-zA-z]+/;
    const phonepattern = /[0-9]+/;
    if (emailpattern.test(valstring)) {
      this.labelTitle = 'Email';
      this.captchaShow = false;
      val.setValidators([Validators.email, Validators.required]);
      val.updateValueAndValidity();
      this.icon = 'email';
    } else if (phonepattern.test(valstring)) {
      this.icon = 'phone';
      this.captchaShow = true;
      if (!valstring.includes('+')) {
        valstring = '+39 ' + valstring;
        val.setValue(valstring);
        console.log(valstring);
      }
      val.setValidators([Validators.pattern('\[+]{1}[0-9]{1,3}[ ]{1}[0-9]{10}'), Validators.required]);
      val.updateValueAndValidity();
      this.labelTitle = 'Telefono';
    }
  }
  get emailPhone() { return this.loginForm.get('emailPhone'); }
  get password() { return this.loginForm.get('password'); }
  ngOnDestroy() {
    this.authSer.medicoLogin = false;
  }
}
