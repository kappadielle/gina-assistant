import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../user';
import { Subscription } from 'rxjs';
import { DocumentReference } from 'angularfire2/firestore';
import { filter, take } from 'rxjs/operators';
import { Prenotazione } from '../prenotazione';
import { Router } from '@angular/router';
import { UserDataService } from '../user-data.service';
import { Medico } from '../medico';
import { CalendarioService } from '../calendario.service';


@Component({
  selector: 'app-visita',
  templateUrl: './visita.component.html',
  styleUrls: ['./visita.component.css']
})
export class VisitaComponent implements OnInit, OnDestroy {
  subNumProgr: Subscription;
  subCodAttesa: Subscription;
  constructor(private router: Router, public dataSer: UserDataService, private calSer: CalendarioService) { }
  maxNumProgressivo: number; // numero massimo visita di oggi
  listaPrenotazioni: Prenotazione[];
  subUserData: Subscription;
  numeroMassimo: Number;
  dati: Prenotazione;
  user: User;
  motivazione = '';
  medico: Medico;
  nominativoArrivaNullo = false;
  listaPrenotazioniCodAttesa: Prenotazione[];
  codiceAttesaMassimo: number;
  ngOnInit() {
    if (this.dataSer.userData) {
      this.medico = this.dataSer.medico;
      this.user = this.dataSer.userData;
      this.calSer.getOrarioSede(this.dataSer.medico.id).pipe(take(1)).subscribe(_ => {
        this.dataSer.sedeOpen = this.calSer.checkStudioAperto();
      });
      this.getNumProgressivo();
      this.getCodiceAttesa();
      if (!this.user.nome) {this.user.nome = ''; }
      if (!this.user.cognome) {this.user.cognome = ''; }
      if (!this.user.cognome || !this.user.nome) {this.nominativoArrivaNullo = true; }
    } else {
      this.subUserData = this.dataSer.userData$.pipe(
        filter(val => val != null), take(1)
      )
      .subscribe(user => {
        this.user = user;
        this.medico = this.dataSer.medico;
        this.calSer.getOrarioSede(this.dataSer.medico.id).pipe(take(1)).subscribe(_ => {
          this.dataSer.sedeOpen = this.calSer.checkStudioAperto();
        });
        this.getNumProgressivo();
        this.getCodiceAttesa();
        if (!this.user.nome) {this.user.nome = ''; }
        if (!this.user.cognome) {this.user.cognome = ''; }
        if (!this.user.cognome || !this.user.nome) {this.nominativoArrivaNullo = true; }
        // funzione
        // this.subUserData.unsubscribe();
      });
    }
    // this.subscritto.unsubscribe();
  }
  getNumProgressivo() {
    console.log(this.dataSer.user);
    this.subNumProgr = this.dataSer.getNumProgressivo().pipe(
        filter(val => val != null)
      ).subscribe(mettiqua => {
        if (mettiqua.length) {
          this.listaPrenotazioni = mettiqua;
          this.numeroMassimo = Number(this.listaPrenotazioni[0].numeroProgressivo); // query dove prende il val max di numProgressivo
        } else {
          this.numeroMassimo = 0;
        }
    });
  }
  getCodiceAttesa() { // prendi l'ultimo codice attesa
    this.subCodAttesa = this.dataSer.getCodiceAttesa().pipe(
        filter(val => val != null)
      ).subscribe(mettiqua => {
        if (mettiqua.length) {
          this.listaPrenotazioniCodAttesa = mettiqua;
          // tslint:disable-next-line:max-line-length
          this.codiceAttesaMassimo = Number(this.listaPrenotazioniCodAttesa[0].codiceAttesa + 1); // query dove prende il val max di codiceAttesa
        } else {
          this.codiceAttesaMassimo = 1;
        }
    });
  }
  aggiungiVisita(motivazione: string) {
    this.dati = {
      UserRef : this.createUserRef(), // NON PRENDE STRING MA REF
      tipo : 'visita',
      TimeIns : null,
      nominativo: this.user.nome + ' ' + this.user.cognome,
      numeroProgressivo: Math.ceil(Number(this.numeroMassimo)) + 1,
      indicazioni: motivazione,
      codFis: this.dataSer.modSala === 'false' ? this.user.codFis : '',
      codiceAttesa: this.codiceAttesaMassimo
    };
    if (this.user.nome.length > 2 && this.user.cognome.length > 3 && this.nominativoArrivaNullo) {
      this.setNomeUser();
      this.setCognomeUser();
    }
    this.router.navigate(['/home']);
    this.dataSer.aggiungiAllaCoda(this.dati);
    this.dataSer.openSnackBar('Hai prenotato una visita', '');
  }
  createUserRef(): DocumentReference {
    return this.dataSer.createUserRef();
  }
  setNomeUser() {
    this.dataSer.setNomeUser(this.user.nome);
  }
  setCognomeUser() {
    this.dataSer.setCognomeUser(this.user.cognome);
  }
  ngOnDestroy(): void {
    if (this.subCodAttesa) { this.subCodAttesa.unsubscribe(); }
    if (this.subNumProgr) { this.subNumProgr.unsubscribe(); }
  }
}
