import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrQueueComponent } from './dr-queue.component';

describe('DrQueueComponent', () => {
  let component: DrQueueComponent;
  let fixture: ComponentFixture<DrQueueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrQueueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
