import { Component, OnInit } from '@angular/core';
import { DBmanagerService } from '../dbmanager.service';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable, Subscription } from 'rxjs';
import { Prenotazione } from '../prenotazione';
import { map, filter, take } from 'rxjs/operators';
import { Medico } from '../medico';
import { UserDataService } from '../user-data.service';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-dr-queue',
  templateUrl: './dr-queue.component.html',
  styleUrls: ['./dr-queue.component.css'],
  animations: [
    trigger('ricette', [
      state('true', style({
        border: '4px solid #ED2E5B'
      })),
      state('false', style({
        border: '1px solid #ED2E5B'
      })),
      transition('* => *', [
        animate(250)
      ]),
    ]),
    trigger('visite', [
      state('true', style({
        border: '4px solid #A2EE2E'
      })),
      state('false', style({
        border: '1px solid #A2EE2E'
      })),
      transition('* => *', [
        animate(250)
      ]),
    ]),
    trigger('nextPren', [
      state('true', style({
        border: '37px solid rgb(54, 134, 252)',
        bottom: '6px',
        right: '36px'
      })),
      state('false', style({
        border: 'solid 0px #3686FC'
      })),
      transition('* => *', [
        animate(150)
      ]),
    ]),
    trigger('prenSgrizza', [
      state('true', style({
        marginLeft: '2000px',
      })),
      transition('* => *', [
        animate(150)
      ]),
    ]),
  ]
})
export class DrQueueComponent implements OnInit {
  elencoCol: AngularFirestoreCollection<Prenotazione>;
  elenco$: Observable<Prenotazione[]>;
  elenco: any[];
  elencoRicette$: Observable<Prenotazione[]>;
  elencoRicette: any[];
  elencoVisite$: Observable<Prenotazione[]>;
  elencoVisite: any[];
  subMed: Subscription;
  subUserData: Subscription;
  medico: Medico;
  lunghezzaCoda: number;
  numVisite: number;
  numRicette: number;
  ricetteEv: string;
  visiteEv: string;
  newPren: string;
  arrayPren: {};
  arrayVisite: {};
  arrayRicette: {};
  primaPrenotazione: string;
  modSala: string;
  photoUrl: any;
  constructor(private DB: DBmanagerService, public dataSer: UserDataService) { }
  onDragStart(event: PointerEvent): void {
    console.log('spostamento da ts');
  }
  onDragMove (event: PointerEvent): void {
    console.log(Math.round(event.clientX) + Math.round(event.clientY));
  }
  onDragEnd (event: PointerEvent): void {
  }
  ngOnInit() { // il campo dataSer.user è riempito con lo stesso oggetto del medico quindi impedisco accesso controllando variabile .medico
    if (this.dataSer.userData) {
      this.medico = this.dataSer.userData;
      this.modSala = this.dataSer.modSala;
      this.getCodaRicette();
      this.getCodaVisite();
      this.getCoda();
    } else {
      this.subUserData = this.dataSer.userData$.pipe(
        filter(val => val != null), take(1)
      )
      .subscribe(user => {
        this.medico = user;
        this.modSala = this.dataSer.modSala;
        this.getCodaRicette();
        this.getCodaVisite();
        this.getCoda();
      });
    }
  }
  deletePrenotazione(id) {
    this.DB.delDocument(`medici/${this.dataSer.userData.id}/sede/${this.dataSer.medico.sedeRef}/coda/${id}`);
  }
  nuovaPren() {
    this.newPren = 'true';
    if (this.elenco[0].payload.doc.data().tipo === 'ricetta') {
      this.dataSer.storage.ref('/' + this.dataSer.medico.id).child(this.elenco[0].payload.doc.data().UserRef.id).delete();
    }
    setTimeout(_ => {this.newPren = 'false'; this.deletePrenotazione(this.elenco[0].payload.doc.id); } , 150); // per animazione
    this.arrayPren[this.elenco[0].payload.doc.id] = 'true';
  }
  nuovaPrenVisita() {
    this.newPren = 'true';
    setTimeout(_ => {this.newPren = 'false'; this.deletePrenotazione(this.elenco[0].payload.doc.id); } , 150); // per animazione
    this.arrayVisite[this.elencoVisite[0].payload.doc.id] = 'true';
  }
  nuovaPrenRicetta() {
    this.newPren = 'true';
    this.dataSer.storage.ref('/' + this.dataSer.medico.id).child(this.elenco[0].payload.doc.data().UserRef.id).delete();
    setTimeout(_ => {this.newPren = 'false'; this.deletePrenotazione(this.elenco[0].payload.doc.id); } , 150); // per animazione
    this.arrayRicette[this.elencoRicette[0].payload.doc.id] = 'true';
  }
  copyStringToClipboard (str) {
    // Create new element
    const el = document.createElement('textarea');
    // Set value (string to be copied)
    el.value = str;
    // Set non-editable to avoid focus and move outside of view
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    // Select text inside element
    el.select();
    // Copy text to clipboard
    document.execCommand('copy');
    // Remove temporary element
    document.body.removeChild(el);
    this.dataSer.openSnackBar('Codice Fiscale copiato', '');
  }
  evidenzia(val: string) {
    if (val === 'ricette') {
      this.ricetteEv = 'true';
      this.visiteEv = 'false';
    } else if (val === 'visite') {
      this.ricetteEv = 'false';
      this.visiteEv = 'true';
    } else if (val === '') {
      this.ricetteEv = 'false';
      this.visiteEv = 'false';
    }
  }
  showImage(photoUrl) {
    this.photoUrl = photoUrl;
  }
  getCoda() {
    // tslint:disable-next-line:max-line-length
    const elencoCol = this.DB.getOrderedCollection('medici/' +  this.medico.id + '/sede/' + this.medico.sedeRef + '/coda', 'numeroProgressivo', 'asc');
    this.elenco$ = elencoCol.snapshotChanges().pipe(
      map(arr => {
        this.elenco = arr;
        this.lunghezzaCoda = arr.length;
        this.numRicette = 0;
        this.numVisite = 0;
        this.arrayPren = {};
        return arr.map(snap => {
          const data = snap.payload.doc.data();
          snap.payload.newIndex = data.numeroProgressivo;
          snap.payload.oldIndex = data.numeroProgressivo;
          const id = snap.payload.doc.id;
          this.arrayPren = {...this.arrayPren, id};
          let photoUrl;
          if (data.tipo === 'ricetta') {
            this.numRicette ++;
            const ref = this.dataSer.storage.ref(this.medico.id + '/' + data.UserRef.id);
            photoUrl = ref.getDownloadURL();
          }
          if (data.tipo === 'visita') {this.numVisite ++; }
          return { id, photoUrl, ...data };
        });
      })
    );
  }
  getCodaVisite() {
    // tslint:disable-next-line:max-line-length
    const elencoCol = this.DB.getOrderedCollection('medici/' +  this.medico.id + '/sede/' + this.medico.sedeRef + '/coda', 'numeroProgressivo', 'asc');
    this.elencoVisite$ = elencoCol.snapshotChanges().pipe(
      map(arr => {
        this.elencoVisite = [];
        this.arrayVisite = {};
        return arr.map(snap => {
          const data = snap.payload.doc.data();
          const id = snap.payload.doc.id;
          let returnVal = {};
          if (data.tipo === 'visita') {
            this.elencoVisite.push(snap);
            this.arrayVisite = {...this.arrayVisite, id};
            returnVal = { id, ...data };
          }
          return returnVal;
        });
      })
    );
  }
  getCodaRicette() {
    // tslint:disable-next-line:max-line-length
    const elencoCol = this.DB.getOrderedCollection('medici/' +  this.medico.id + '/sede/' + this.medico.sedeRef + '/coda', 'TimeIns', 'asc');
    this.elencoRicette$ = elencoCol.snapshotChanges().pipe(
      map(arr => {
        this.elencoRicette = [];
        this.arrayRicette = {};
        return arr.map(snap => {
          const data = snap.payload.doc.data();
          const id = snap.payload.doc.id;
          let returnVal = {};
          let photoUrl;
          if (data.tipo === 'ricetta') {
            this.elencoRicette.push(snap);
            const ref = this.dataSer.storage.ref(this.medico.id + '/' + data.UserRef.id);
            photoUrl = ref.getDownloadURL();
            this.arrayRicette = {...this.arrayRicette, id};
            returnVal = { id, photoUrl, ...data };
          }
          return returnVal;
        });
      })
    );
  }
  update(e) {
    if (e.newIndex > e.oldIndex) {
      e.newIndex = e.newIndex + 0.00001;
      // tslint:disable-next-line:max-line-length
      this.DB.updateDocument('medici/' +  this.medico.id + '/sede/' + this.medico.sedeRef + '/coda/' + e.idDoc, { numeroProgressivo: e.newIndex});
    } else if (e.newIndex < e.oldIndex) {
      e.newIndex = e.newIndex - 0.00001;
      // tslint:disable-next-line:max-line-length
      this.DB.updateDocument('medici/' +  this.medico.id + '/sede/' + this.medico.sedeRef + '/coda/' + e.idDoc, { numeroProgressivo: e.newIndex});
    }
  }
}
