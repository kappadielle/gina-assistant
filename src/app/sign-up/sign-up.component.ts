import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';
import { Subscription } from 'rxjs';
import { FormBuilder, Validators } from '@angular/forms';
import {AngularFirestoreCollection } from 'angularfire2/firestore';
import { AutenticationService } from '../autentication.service';
import { comparePasswordsValidator } from './comparePasswordValidator';
import { WindowService } from '../window.service';
import { TooltipPosition } from '@angular/material';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  // userForm: FormGroup;
  user: any;
  tooltipPosition: TooltipPosition = 'right';
  icon: string;
  labelTitle = 'Email o Telefono';
  UserCol: AngularFirestoreCollection;
  windowRef: any;
  captchaShow = false;
  subWindow: Subscription;
  verificationCode: string;
  hide = false;
  hide2 = false;
  constructor(private win: WindowService, public authSer: AutenticationService, private fb: FormBuilder) { }
  signUpForm = this.fb.group({
    emailPhone:   ['', {
      validators: Validators.required,
      updateOn: 'blur'
    }],
    password:     ['', [Validators.required, Validators.minLength(6), Validators.maxLength(30),
                        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$')]],
    passwordConf: ['', [Validators.required]],
    // codFis:       ['', [Validators.pattern('[A-Za-z]{6}[0-9]{2}[A-Za-z]{1}[0-9]{2}[A-Za-z]{1}[0-9]{3}[A-Za-z]{1}')]]
  }, { validator: comparePasswordsValidator });

  ngOnInit() {
      this.windowRef = this.win.windowRef;
      this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
      this.windowRef.recaptchaVerifier.render();
  }
  googleLogin() {
    this.authSer.googleLogin();
  }
  phoneSendLoginCode() {
    const num = this.signUpForm.value['emailPhone'];
    const appVerifier = this.windowRef.recaptchaVerifier;
    // num = '+393468302708';
    this.authSer.sendLoginCode(num, appVerifier, this.windowRef);
  }
  verifyLoginCode() {
    // const codFis = this.signUpForm.value['codFis'];
    this.authSer.verifyLoginCode(this.verificationCode);
  }
  signUp() {
    const emailPhone = this.signUpForm.value['emailPhone'];
    const password = this.signUpForm.value['password'];
    // const CodFis = this.signUpForm.value['codFis'];
    if (this.icon === 'email' ) {
      this.authSer.emailSignUp(emailPhone, password);
    } else if (this.icon === 'phone') {
      this.verifyLoginCode();
    }
  }
  updateValidators() {
    const val = this.signUpForm.get('emailPhone');
    let valstring = val.value;
    const emailpattern = /[a-zA-z]+/;
    const phonepattern = /[0-9]+/;
    if (emailpattern.test(valstring)) {
      this.labelTitle = 'Email';
      this.captchaShow = false;
      val.setValidators([Validators.email, Validators.required]);
      val.updateValueAndValidity();
      this.icon = 'email';
    } else if (phonepattern.test(valstring)) {
      this.icon = 'phone';
      this.captchaShow = true;
      if (!valstring.includes('+')) {
        valstring = '+39 ' + valstring;
        val.setValue(valstring);
        console.log(valstring);
      }
      val.setValidators([Validators.pattern('\[+]{1}[0-9]{1,3}[ ]{1}[0-9]{10}'), Validators.required]);
      val.updateValueAndValidity();
      this.labelTitle = 'Telefono';
    }
  }
  // lasciali invariati al massimo fai ng onchange come sopra ^^^
  get emailPhone() { return this.signUpForm.get('emailPhone'); }
  get telefono() { return this.signUpForm.get('telefono'); }
  get email() { console.log(this.signUpForm.get('email').value); return this.signUpForm.get('email'); }
  get password() { return this.signUpForm.get('password'); }
  get passwordConf() { return this.signUpForm.get('passwordConf'); }
  // get codFis() { return this.signUpForm.get('codFis'); }
}

