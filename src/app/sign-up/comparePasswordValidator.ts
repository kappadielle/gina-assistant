import { ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';

export const comparePasswordsValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const password = control.get('password');
    const Cpassword = control.get('passwordConf');
    return Cpassword && password && Cpassword.value !== password.value ? { 'comparePasswords': true } : null;
};
