import { Directive, AfterViewInit, Input, Output, ElementRef, EventEmitter } from '@angular/core';
import { Sortable } from '@shopify/draggable';
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[sortable]'
})
export class SortableDirective implements AfterViewInit {
  sortable: Sortable;
  constructor(private el: ElementRef) { }
  @Input() data: any[];
  @Output() stop = new EventEmitter();

  ngAfterViewInit() {
    this.sortable = new Sortable(this.el.nativeElement, { draggable : '.pren-container-dyn' });
    this.sortable.on('sortable:stop', e => this.handleStop(e));
  }
  handleStop(e) {
    const { newIndex, oldIndex } = e; // old e new indexes sono uguali porcodio
    const next = this.data;
    this.stop.emit({ newIndex: next[newIndex].numeroProgressivo,
                    idDoc: next[oldIndex].id,
                    oldIndex: next[oldIndex].numeroProgressivo});
    const moved = next.splice(oldIndex, 1);
    next.splice(newIndex, 0, moved[0]);
  }
}
